<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chef form</title>
</head>
<body>
    <c:forEach items="${orderJson}" var="order" varStatus="i">

            <form name="FinishedOrder" method="GET" action="<c:url value='/api/chef/finishOrder'/>">
                <input type="hidden" name="chefId" value="i'm a chef in the PizzaChef">
                        ${i.count}
                        <p><textarea name="data" id="data" cols="48" rows="8">
                        ${order}
                        </textarea>
                        </p>
                <input type='submit' name='save' id='save' value='Save'>
            </form>

    </c:forEach>

</body>
</html>
