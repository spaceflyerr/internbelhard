<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>New order</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
</head>
<body>
<select name="pizzaSize" size="1">
    <c:forEach items="${pizzaSize}" var="size" varStatus="count">
        <option value="${count.count}">{"id":${size.id}, "diameter":${size.diameter}, "name":"${size.name}"}</option>
    </c:forEach>
</select>
<br>

<select name="pizzaType" size="1">
    <c:forEach items="${pizzaType}" var="type" varStatus="count">
        <option value="${count.count}">{"id":${type.id}, "description":"${type.description}", "name":"${type.name}"}</option>
    </c:forEach>
</select>
<br>

<select name="ingredient" size="1">
    <c:forEach items="${ingredient}" var="item" varStatus="count">
        <option value="${count.count}">{"id":${item.id}, "category":{"id":${item.category.id}, "name":"${item.category.name}"}, "name":"${item.name}"}</option>
    </c:forEach>
</select>

<select name="ingredient2" size="1">
    <c:forEach items="${ingredient}" var="item" varStatus="count">
        <option value="${count.count}">{"id":${item.id}, "category":{"id":${item.category.id}, "name":"${item.category.name}"}, "name":"${item.name}"}</option>
    </c:forEach>
</select>

<select name="ingredient3" size="1">
    <c:forEach items="${ingredient}" var="item" varStatus="count">
        <option value="${count.count}">{"id":${item.id}, "category":{"id":${item.category.id}, "name":"${item.category.name}"}, "name":"${item.name}"}</option>
    </c:forEach>
</select>

    <form name="AddNewOrder" method="GET" action="<c:url value='/api/addOrder'/>">
        <input type="hidden" name="client_id" value="1">
        <p><textarea name="order" id="order" cols="48" rows="8">
            ${Order}
        </textarea>
        </p>
        <input type='submit' name='save' id='save' value='Save'>
    </form>

</body>
</html>
