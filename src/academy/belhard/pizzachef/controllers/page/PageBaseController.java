package academy.belhard.pizzachef.controllers.page;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static academy.belhard.pizzachef.constants.ControllerConstants.VIEW_NAME_TEMPLATE;

public abstract class PageBaseController extends HttpServlet {
    private String name = "";

    public PageBaseController(String name){
        this.name = name;
    }

    protected abstract void performTask(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

    protected void render(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(String.format(VIEW_NAME_TEMPLATE, this.name)).forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        performTask(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        performTask(req,resp);
    }
}
