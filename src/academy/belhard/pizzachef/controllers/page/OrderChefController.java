package academy.belhard.pizzachef.controllers.page;

import academy.belhard.pizzachef.dao.AbstractDAO;
import academy.belhard.pizzachef.dao.OrderDAO;
import academy.belhard.pizzachef.jsonobjects.OrderConvert;
import academy.belhard.pizzachef.model.entity.OrderEntity;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(name = "OrderChefController", urlPatterns = "/getOrderChef")
public class OrderChefController extends PageBaseController {
    private static final String NAME = "ChefOrder";
    private static final String ORDER_JSON = "orderJson";
    private static final String PARAMETER_SEARCH_FINISHED = "finished";

    public OrderChefController(){
        super(NAME);
    }
    @Override
    protected void performTask(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AbstractDAO<OrderEntity> oDAO = new OrderDAO();

        Map<String, Object> map = new HashMap();
        map.put(PARAMETER_SEARCH_FINISHED, false);

        List<OrderEntity> oList = oDAO.getRecordList(map);
        List<String> ojList = new ArrayList<>();
        Gson gson = new Gson();
        for (OrderEntity orderEntity : oList) {
            OrderConvert oc = new OrderConvert(orderEntity);
            ojList.add(gson.toJson(oc.getOrderJson()));
        }
        request.setAttribute(ORDER_JSON, ojList.toArray());
        this.render(request, response);
    }
}
