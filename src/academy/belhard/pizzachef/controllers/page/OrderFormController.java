package academy.belhard.pizzachef.controllers.page;

import academy.belhard.pizzachef.dao.*;
import academy.belhard.pizzachef.jsonobjects.OrderConvert;
import academy.belhard.pizzachef.model.entity.IngredientEntity;
import academy.belhard.pizzachef.model.entity.OrderEntity;
import academy.belhard.pizzachef.model.entity.PizzaSizeEntity;
import academy.belhard.pizzachef.model.entity.PizzaTypeEntity;
import academy.belhard.pizzachef.model.json.OrderJson;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "OrderFormController", urlPatterns = "/newOrderForm")
public class OrderFormController extends PageBaseController {
    private static final String NAME = "NewOrder";
    private static final String PIZZA_SIZE = "pizzaSize";
    private static final String PIZZA_TYPE = "pizzaType";
    private static final String INGREDIENT = "ingredient";
    private static final String ORDER = "Order";

    public OrderFormController(){
        super(NAME);
    }

    @Override
    protected void performTask(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AbstractDAO<PizzaSizeEntity> psDAO = new PizzaSizeDAO();
        AbstractDAO<PizzaTypeEntity> ptDAO = new PizzaTypeDAO();
        AbstractDAO<IngredientEntity> iDAO = new IngredientDAO();
        AbstractDAO<OrderEntity> oDAO = new OrderDAO();

        List<PizzaSizeEntity> psList = psDAO.getRecordList();
        List<PizzaTypeEntity> ptList = ptDAO.getRecordList();
        List<IngredientEntity> iList = iDAO.getRecordList();
        OrderEntity order = oDAO.getRecordById(1);
        OrderConvert oc = new OrderConvert(order);
        OrderJson oj = oc.getOrderJson();
        Gson gson = new Gson();
        String json = gson.toJson(oj);

        request.setAttribute(ORDER, json);
        request.setAttribute(PIZZA_SIZE, psList);
        request.setAttribute(PIZZA_TYPE, ptList);
        request.setAttribute(INGREDIENT, iList);

        this.render(request, response);

    }
}
