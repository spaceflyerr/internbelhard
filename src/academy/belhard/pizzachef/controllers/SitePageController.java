package academy.belhard.pizzachef.controllers;

import academy.belhard.pizzachef.controllers.page.PageBaseController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "SiteControllerPage", urlPatterns = "/site")
public class SitePageController extends PageBaseController {

    private static final String NAME = "Site";
    private static final String ATTRIBUTE_NAME = "test";
    private static final String ATTRIBUTE_VALUE = "this is a test";

    public SitePageController(){
        super(NAME);
    }

    @Override
    protected void performTask(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute(ATTRIBUTE_NAME, ATTRIBUTE_VALUE);
		this.render(request, response);
    }
}
