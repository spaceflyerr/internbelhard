package academy.belhard.pizzachef.controllers.api.chef;

import academy.belhard.pizzachef.constants.ControllerConstants;
import academy.belhard.pizzachef.controllers.api.ApiBaseController;
import academy.belhard.pizzachef.dao.AbstractDAO;
import academy.belhard.pizzachef.dao.OrderDAO;
import academy.belhard.pizzachef.jsonobjects.OrderConvert;
import academy.belhard.pizzachef.model.entity.OrderEntity;
import academy.belhard.pizzachef.model.json.OrderJson;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(name = "UnfinishedOrderListController", urlPatterns = "/api/chef/getOrderList")
public class UnfinishedOrderListController extends ApiBaseController {
    private static final String PARAMETER_SEARCH_FINISHED = "finished";

    @Override
    protected void apiFunction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AbstractDAO<OrderEntity> order = new OrderDAO();
        Map<String, Object> map = new HashMap();
        map.put(PARAMETER_SEARCH_FINISHED, false);
        List<OrderEntity> orderEntityList = order.getRecordList(map);

        List<OrderJson> orderJsonList = new ArrayList<>();
        for (OrderEntity orderEntity : orderEntityList) {
            OrderConvert orderRequest = new OrderConvert(orderEntity);
            orderJsonList.add(orderRequest.getOrderJson());
        }

        Gson gson = new Gson();
        String jsonResponse = gson.toJson(orderJsonList);

        response.setContentType(ControllerConstants.CONTENT_TYPE_JSON);
        response.setCharacterEncoding(ControllerConstants.CHAR_ENCODING_UTF8);
        response.getWriter().write(jsonResponse);
    }
}
