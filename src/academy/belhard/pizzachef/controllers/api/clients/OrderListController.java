package academy.belhard.pizzachef.controllers.api.clients;

import academy.belhard.pizzachef.constants.ControllerConstants;
import academy.belhard.pizzachef.controllers.api.ApiBaseController;
import academy.belhard.pizzachef.dao.AbstractDAO;
import academy.belhard.pizzachef.dao.ClientDAO;
import academy.belhard.pizzachef.dao.OrderDAO;
import academy.belhard.pizzachef.jsonobjects.OrderConvert;
import academy.belhard.pizzachef.model.entity.ClientEntity;
import academy.belhard.pizzachef.model.entity.OrderEntity;
import academy.belhard.pizzachef.model.json.OrderJson;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static academy.belhard.pizzachef.constants.DAOConstants.PARAMETER_CLIENT;

@WebServlet(name = "OrderListController", urlPatterns = "/api/getOrderList")
public class OrderListController extends ApiBaseController {

    @Override
    protected void apiFunction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String clientId = request.getParameter(ControllerConstants.PARAMETER_NAME_IN_REQUEST_FOR_ALL_ORDERS);

        AbstractDAO<ClientEntity> clientDAO = new ClientDAO();
        Map<String, Object> map = new HashMap();
        map.put(PARAMETER_CLIENT, clientDAO.getRecordById(Long.valueOf(clientId)));

        AbstractDAO<OrderEntity> orderDAO = new OrderDAO();
        List<OrderEntity> orderEntityList = orderDAO.getRecordList(map);

        List<OrderJson> orderJsonList = new ArrayList<>();
        for (OrderEntity orderEntity : orderEntityList) {
            OrderConvert orderResponse = new OrderConvert(orderEntity);
            orderJsonList.add(orderResponse.getOrderJson());
        }

        Gson gson = new Gson();
        String jsonResponse = gson.toJson(orderJsonList);

        response.setContentType(ControllerConstants.CONTENT_TYPE_JSON);
        response.setCharacterEncoding(ControllerConstants.CHAR_ENCODING_UTF8);
        response.getWriter().write(jsonResponse);
    }
}
