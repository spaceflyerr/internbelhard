package academy.belhard.pizzachef.controllers.api.clients;

import academy.belhard.pizzachef.constants.ControllerConstants;
import academy.belhard.pizzachef.constants.ErrorConstants;
import academy.belhard.pizzachef.controllers.api.ApiBaseController;
import academy.belhard.pizzachef.dao.*;
import academy.belhard.pizzachef.jsonobjects.OrderConvert;
import academy.belhard.pizzachef.model.entity.*;
import academy.belhard.pizzachef.model.json.ErrorJson;
import academy.belhard.pizzachef.model.json.OrderJson;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@WebServlet(name = "NewOrderController", urlPatterns = "/api/addOrder")
public class NewOrderController extends ApiBaseController {
    private static final String ORDER_JSON_NAME_IN_REQUEST = "order";
    private static final String CLIENT_ID_IN_REQUEST = "client_id";
    private static final String PARAMETER_SEARCH_DIAMETER = "diameter";
    private static final String PARAMETER_SEARCH_NAME = "name";

    @Override
    protected void apiFunction(HttpServletRequest request, HttpServletResponse response) throws ServletException,
                                                                                                IOException {
        String jsonRequest = request.getParameter(ORDER_JSON_NAME_IN_REQUEST);
        String clientId = request.getParameter(CLIENT_ID_IN_REQUEST);

        Gson gson = new Gson();
        OrderJson orderJsonRequest = gson.fromJson(jsonRequest, OrderJson.class);

        AbstractDAO<ClientEntity> clientDAO = new ClientDAO();
        ClientEntity clientEntity = clientDAO.getRecordById(Long.valueOf(clientId));

        OrderConvert orderRequest = new OrderConvert(orderJsonRequest);
        OrderEntity orderEntity = orderRequest.getOrderEntity();
        orderEntity.setClient(clientEntity);

        List<PizzaEntity> list = orderEntity.getListOfPizzas();
        for (PizzaEntity pe : list) {
            AbstractDAO<PizzaSizeEntity> psDAO = new PizzaSizeDAO();
            AbstractDAO<PizzaTypeEntity> ptDAO = new PizzaTypeDAO();

            Map<String, Object> mapParameter = new HashMap<>();
            mapParameter.put(PARAMETER_SEARCH_DIAMETER, pe.getPizzaSize().getDiameter());

            pe.setPizzaSize(psDAO.getRecordList(mapParameter).get(0));
            mapParameter.clear();
            mapParameter.put(PARAMETER_SEARCH_NAME, pe.getPizzaType().getName());
            pe.setPizzaType(ptDAO.getRecordList(mapParameter).get(0));

            Map<Byte, IngredientEntity> map = pe.getIngredientMap();
            for (Map.Entry<Byte, IngredientEntity> entry : map.entrySet()) {
                AbstractDAO<IngredientEntity> iDAO = new IngredientDAO();
                mapParameter.clear();
                mapParameter.put(PARAMETER_SEARCH_NAME, entry.getValue().getName());
                entry.setValue(iDAO.getRecordList(mapParameter).get(0));
            }

        }


        AbstractDAO<OrderEntity> orderDAO = new OrderDAO();
        OrderEntity orderEntityAfterAdd = orderDAO.addRecord(orderEntity);
        String jsonResponse = getJsonResponse(orderEntityAfterAdd);

        response.setContentType(ControllerConstants.CONTENT_TYPE_JSON);
        response.setCharacterEncoding(ControllerConstants.CHAR_ENCODING_UTF8);
        response.getWriter().write(jsonResponse);
    }

    private String getJsonResponse(OrderEntity orderEntityAfterAdd) {
        Gson gson = new Gson();
        String jsonResponse;
        if (orderEntityAfterAdd != null) {
            OrderConvert orderResponse = new OrderConvert(orderEntityAfterAdd);
            jsonResponse = gson.toJson(orderResponse.getOrderJson());
        } else {
            ErrorJson error = new ErrorJson(ErrorConstants.CODE_OF_ERROR_ADD_ORDER,
                                            ErrorConstants.MESSAGE_OF_ERROR_ADD_ORDER);
            jsonResponse = gson.toJson(error);
        }
        return jsonResponse;
    }
}
