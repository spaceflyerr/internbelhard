package academy.belhard.pizzachef.controllers.api.clients;

import academy.belhard.pizzachef.constants.ControllerConstants;
import academy.belhard.pizzachef.controllers.api.ApiBaseController;
import academy.belhard.pizzachef.dao.AbstractDAO;
import academy.belhard.pizzachef.dao.IngredientDAO;
import academy.belhard.pizzachef.jsonobjects.IngredientConvert;
import academy.belhard.pizzachef.model.entity.IngredientEntity;
import academy.belhard.pizzachef.model.json.IngredientJson;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "IngredientListController", urlPatterns = "/api/getIngredientList")
public class IngredientListController extends ApiBaseController {
    @Override
    protected void apiFunction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AbstractDAO<IngredientEntity> ingredientDAO = new IngredientDAO();
        List<IngredientEntity> ingredientEntityList = ingredientDAO.getRecordList();

        List<IngredientJson> ingredientJsonList = new ArrayList<>();
        for (IngredientEntity ingredientEntity : ingredientEntityList) {
            IngredientConvert ingredientRequest = new IngredientConvert(ingredientEntity);
            ingredientJsonList.add(ingredientRequest.getIngredientJson());
        }

        Gson gson = new Gson();
        String jsonResponse = gson.toJson(ingredientJsonList);

        response.setContentType(ControllerConstants.CONTENT_TYPE_JSON);
        response.setCharacterEncoding(ControllerConstants.CHAR_ENCODING_UTF8);
        response.getWriter().write(jsonResponse);
    }
}
