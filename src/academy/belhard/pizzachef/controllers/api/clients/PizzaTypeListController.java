package academy.belhard.pizzachef.controllers.api.clients;

import academy.belhard.pizzachef.constants.ControllerConstants;
import academy.belhard.pizzachef.controllers.api.ApiBaseController;
import academy.belhard.pizzachef.dao.AbstractDAO;
import academy.belhard.pizzachef.dao.PizzaTypeDAO;
import academy.belhard.pizzachef.jsonobjects.PizzaTypeConvert;
import academy.belhard.pizzachef.model.entity.PizzaTypeEntity;
import academy.belhard.pizzachef.model.json.PizzaTypeJson;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "PizzaTypeListController", urlPatterns = "/api/getPizzaTypeList")
public class PizzaTypeListController extends ApiBaseController {
    @Override
    protected void apiFunction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AbstractDAO<PizzaTypeEntity> pizzaTypeDAO = new PizzaTypeDAO();
        List<PizzaTypeEntity> pizzaTypeEntityList = pizzaTypeDAO.getRecordList();

        List<PizzaTypeJson> pizzaTypeJsonList = new ArrayList<>();
        for (PizzaTypeEntity pizzaTypeEntity : pizzaTypeEntityList) {
            PizzaTypeConvert pizzaTypeRequest = new PizzaTypeConvert(pizzaTypeEntity);
            pizzaTypeJsonList.add(pizzaTypeRequest.getPizzaTypeJson());
        }

        Gson gson = new Gson();
        String jsonResponse = gson.toJson(pizzaTypeJsonList);

        response.setContentType(ControllerConstants.CONTENT_TYPE_JSON);
        response.setCharacterEncoding(ControllerConstants.CHAR_ENCODING_UTF8);
        response.getWriter().write(jsonResponse);
    }
}
