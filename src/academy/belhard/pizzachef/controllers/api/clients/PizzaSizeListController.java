package academy.belhard.pizzachef.controllers.api.clients;

import academy.belhard.pizzachef.constants.ControllerConstants;
import academy.belhard.pizzachef.controllers.api.ApiBaseController;
import academy.belhard.pizzachef.dao.AbstractDAO;
import academy.belhard.pizzachef.dao.PizzaSizeDAO;
import academy.belhard.pizzachef.jsonobjects.PizzaSizeConvert;
import academy.belhard.pizzachef.model.entity.PizzaSizeEntity;
import academy.belhard.pizzachef.model.json.PizzaSizeJson;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "PizzaSizeListController", urlPatterns = "/api/getPizzaSizeList")
public class PizzaSizeListController extends ApiBaseController {
    @Override
    protected void apiFunction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AbstractDAO<PizzaSizeEntity> pizzaSizeDAO = new PizzaSizeDAO();
        List<PizzaSizeEntity> pizzaSizeEntityList = pizzaSizeDAO.getRecordList();

        List<PizzaSizeJson> pizzaSizeJsonList = new ArrayList<>();
        for (PizzaSizeEntity pizzaSizeEntity : pizzaSizeEntityList) {
            PizzaSizeConvert pizzaSizeRequest = new PizzaSizeConvert(pizzaSizeEntity);
            pizzaSizeJsonList.add(pizzaSizeRequest.getPizzaSizeJson());
        }

        Gson gson = new Gson();
        String jsonResponse = gson.toJson(pizzaSizeJsonList);

        response.setContentType(ControllerConstants.CONTENT_TYPE_JSON);
        response.setCharacterEncoding(ControllerConstants.CHAR_ENCODING_UTF8);
        response.getWriter().write(jsonResponse);
    }
}
