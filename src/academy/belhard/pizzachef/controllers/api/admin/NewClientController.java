package academy.belhard.pizzachef.controllers.api.admin;

import academy.belhard.pizzachef.constants.ControllerConstants;
import academy.belhard.pizzachef.constants.ErrorConstants;
import academy.belhard.pizzachef.controllers.api.ApiBaseController;
import academy.belhard.pizzachef.dao.AbstractDAO;
import academy.belhard.pizzachef.dao.ClientDAO;
import academy.belhard.pizzachef.jsonobjects.ClientConvert;
import academy.belhard.pizzachef.model.entity.ClientEntity;
import academy.belhard.pizzachef.model.json.ClientJson;
import academy.belhard.pizzachef.model.json.ErrorJson;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static academy.belhard.pizzachef.constants.ControllerConstants.CLIENT_JSON_NAME_IN_REQUEST;

@WebServlet(name = "NewClientController", urlPatterns = "/api/admin/addClient")
public class NewClientController extends ApiBaseController {
    @Override
    protected void apiFunction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String jsonRequest = request.getParameter(CLIENT_JSON_NAME_IN_REQUEST);

        Gson gson = new Gson();
        ClientJson clientJsonRequest = gson.fromJson(jsonRequest, ClientJson.class);

        ClientConvert clientRequest = new ClientConvert(clientJsonRequest);
        ClientEntity clientEntity = clientRequest.getClientEntity();

        AbstractDAO<ClientEntity> clientDAO = new ClientDAO();
        ClientEntity clientEntityAfterAdd = clientDAO.addRecord(clientEntity);

        String jsonResponse = getJsonResponse(clientEntityAfterAdd);

        response.setContentType(ControllerConstants.CONTENT_TYPE_JSON);
        response.setCharacterEncoding(ControllerConstants.CHAR_ENCODING_UTF8);
        response.getWriter().write(jsonResponse);
    }

    private String getJsonResponse(ClientEntity clientEntityAfterAdd) {
        Gson gson = new Gson();
        String jsonResponse;
        if (clientEntityAfterAdd != null) {
            ClientConvert clientResponse = new ClientConvert(clientEntityAfterAdd);
            jsonResponse = gson.toJson(clientResponse.getClientJson());
        } else {
            ErrorJson error = new ErrorJson(ErrorConstants.CODE_OF_ERROR_ADD_CLIENT,
                    ErrorConstants.MESSAGE_OF_ERROR_ADD_CLIENT);
            jsonResponse = gson.toJson(error);
        }
        return jsonResponse;
    }

}
