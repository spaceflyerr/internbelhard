package academy.belhard.pizzachef.controllers.api.admin;

import academy.belhard.pizzachef.constants.ControllerConstants;
import academy.belhard.pizzachef.constants.ErrorConstants;
import academy.belhard.pizzachef.controllers.api.ApiBaseController;
import academy.belhard.pizzachef.dao.AbstractDAO;
import academy.belhard.pizzachef.dao.PizzaTypeDAO;
import academy.belhard.pizzachef.jsonobjects.PizzaTypeConvert;
import academy.belhard.pizzachef.model.entity.PizzaTypeEntity;
import academy.belhard.pizzachef.model.json.ErrorJson;
import academy.belhard.pizzachef.model.json.PizzaTypeJson;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static academy.belhard.pizzachef.constants.ControllerConstants.PIZZA_TYPE_JSON_NAME_IN_REQUEST;

@WebServlet(name = "NewPizzaTypeController", urlPatterns = "/api/admin/addPizzaType")
public class NewPizzaTypeController extends ApiBaseController {
    @Override
    protected void apiFunction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String jsonRequest = request.getParameter(PIZZA_TYPE_JSON_NAME_IN_REQUEST);

        Gson gson = new Gson();
        PizzaTypeJson pizzaTypeJsonRequest;
        pizzaTypeJsonRequest = gson.fromJson(jsonRequest, PizzaTypeJson.class);

        PizzaTypeConvert pizzaTypeRequest = new PizzaTypeConvert(pizzaTypeJsonRequest);
        PizzaTypeEntity pizzaTypeEntity = pizzaTypeRequest.getPizzaTypeEntity();

        AbstractDAO<PizzaTypeEntity> pizzaTypeDAO = new PizzaTypeDAO();
        PizzaTypeEntity pizzaTypeEntityAfterAdd = pizzaTypeDAO.addRecord(pizzaTypeEntity);

        String jsonResponse = getJsonResponse(pizzaTypeEntityAfterAdd);

        response.setContentType(ControllerConstants.CONTENT_TYPE_JSON);
        response.setCharacterEncoding(ControllerConstants.CHAR_ENCODING_UTF8);
        response.getWriter().write(jsonResponse);
    }

    private String getJsonResponse(PizzaTypeEntity pizzaTypeEntityAfterAdd) {
        Gson gson = new Gson();
        String jsonResponse;
        if (pizzaTypeEntityAfterAdd != null) {
            PizzaTypeConvert pizzaTypeResponse = new PizzaTypeConvert(pizzaTypeEntityAfterAdd);
            jsonResponse = gson.toJson(pizzaTypeResponse.getPizzaTypeJson());
        } else {
            ErrorJson error = new ErrorJson(ErrorConstants.CODE_OF_ERROR_ADD_PIZZA_TYPE,
                    ErrorConstants.MESSAGE_OF_ERROR_ADD_PIZZA_TYPE);
            jsonResponse = gson.toJson(error);
        }
        return jsonResponse;
    }
}
