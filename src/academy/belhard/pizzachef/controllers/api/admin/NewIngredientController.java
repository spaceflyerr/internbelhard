package academy.belhard.pizzachef.controllers.api.admin;

import academy.belhard.pizzachef.constants.ControllerConstants;
import academy.belhard.pizzachef.constants.ErrorConstants;
import academy.belhard.pizzachef.controllers.api.ApiBaseController;
import academy.belhard.pizzachef.dao.AbstractDAO;
import academy.belhard.pizzachef.dao.IngredientDAO;
import academy.belhard.pizzachef.jsonobjects.IngredientConvert;
import academy.belhard.pizzachef.model.entity.IngredientEntity;
import academy.belhard.pizzachef.model.json.ErrorJson;
import academy.belhard.pizzachef.model.json.IngredientJson;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static academy.belhard.pizzachef.constants.ControllerConstants.INGREDIENT_JSON_NAME_IN_REQUEST;

@WebServlet(name = "NewIngredientController", urlPatterns = "/api/admin/addIngredient")
public class NewIngredientController extends ApiBaseController {
    @Override
    protected void apiFunction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String jsonRequest = request.getParameter(INGREDIENT_JSON_NAME_IN_REQUEST);

        Gson gson = new Gson();
        IngredientJson ingredientJsonRequest = gson.fromJson(jsonRequest, IngredientJson.class);

        IngredientConvert ingredientRequest = new IngredientConvert(ingredientJsonRequest);
        IngredientEntity ingredientEntity = ingredientRequest.getIngredientEntity();

        AbstractDAO<IngredientEntity> ingredientDAO = new IngredientDAO();
        IngredientEntity ingredientEntityAfterAdd = ingredientDAO.addRecord(ingredientEntity);

        String jsonResponse = getJsonResponse(ingredientEntityAfterAdd);

        response.setContentType(ControllerConstants.CONTENT_TYPE_JSON);
        response.setCharacterEncoding(ControllerConstants.CHAR_ENCODING_UTF8);
        response.getWriter().write(jsonResponse);
    }

    private String getJsonResponse(IngredientEntity ingredientEntityAfterAdd) {
        Gson gson = new Gson();
        String jsonResponse;
        if (ingredientEntityAfterAdd != null) {
            IngredientConvert ingredientResponse = new IngredientConvert(ingredientEntityAfterAdd);
            jsonResponse = gson.toJson(ingredientResponse.getIngredientJson());
        } else {
            ErrorJson error = new ErrorJson(ErrorConstants.CODE_OF_ERROR_ADD_INGREDIENT,
                    ErrorConstants.MESSAGE_OF_ERROR_ADD_INGREDIENT);
            jsonResponse = gson.toJson(error);
        }
        return jsonResponse;
    }

}
