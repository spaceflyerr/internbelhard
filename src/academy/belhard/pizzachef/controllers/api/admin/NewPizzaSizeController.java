package academy.belhard.pizzachef.controllers.api.admin;

import academy.belhard.pizzachef.constants.ControllerConstants;
import academy.belhard.pizzachef.constants.ErrorConstants;
import academy.belhard.pizzachef.controllers.api.ApiBaseController;
import academy.belhard.pizzachef.dao.AbstractDAO;
import academy.belhard.pizzachef.dao.PizzaSizeDAO;
import academy.belhard.pizzachef.jsonobjects.PizzaSizeConvert;
import academy.belhard.pizzachef.model.entity.PizzaSizeEntity;
import academy.belhard.pizzachef.model.json.ErrorJson;
import academy.belhard.pizzachef.model.json.PizzaSizeJson;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static academy.belhard.pizzachef.constants.ControllerConstants.PIZZA_SIZE_JSON_NAME_IN_REQUEST;

@WebServlet(name = "NewPizzaSizeController", urlPatterns = "/api/admin/addPizzaSize")
public class NewPizzaSizeController extends ApiBaseController {
    @Override
    protected void apiFunction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String jsonRequest = request.getParameter(PIZZA_SIZE_JSON_NAME_IN_REQUEST);

        Gson gson = new Gson();
        PizzaSizeJson pizzaSizeJsonRequest = gson.fromJson(jsonRequest, PizzaSizeJson.class);

        PizzaSizeConvert pizzaSizeRequest = new PizzaSizeConvert(pizzaSizeJsonRequest);
        PizzaSizeEntity pizzaSizeEntity = pizzaSizeRequest.getPizzaSizeEntity();

        AbstractDAO<PizzaSizeEntity> pizzaSizeDAO = new PizzaSizeDAO();
        PizzaSizeEntity pizzaSizeEntityAfterAdd = pizzaSizeDAO.addRecord(pizzaSizeEntity);

        String jsonResponse = getJsonResponse(pizzaSizeEntityAfterAdd);

        response.setContentType(ControllerConstants.CONTENT_TYPE_JSON);
        response.setCharacterEncoding(ControllerConstants.CHAR_ENCODING_UTF8);
        response.getWriter().write(jsonResponse);
    }

    private String getJsonResponse(PizzaSizeEntity pizzaSizeEntityAfterAdd) {
        Gson gson = new Gson();
        String jsonResponse;
        if (pizzaSizeEntityAfterAdd != null) {
            PizzaSizeConvert pizzaSizeResponse = new PizzaSizeConvert(pizzaSizeEntityAfterAdd);
            jsonResponse = gson.toJson(pizzaSizeResponse.getPizzaSizeJson());
        } else {
            ErrorJson error = new ErrorJson(ErrorConstants.CODE_OF_ERROR_ADD_PIZZA_SIZE,
                    ErrorConstants.MESSAGE_OF_ERROR_ADD_PIZZA_SIZE);
            jsonResponse = gson.toJson(error);
        }
        return jsonResponse;
    }
}
