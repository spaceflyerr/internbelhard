package academy.belhard.pizzachef.controllers.api.admin;

import academy.belhard.pizzachef.constants.ControllerConstants;
import academy.belhard.pizzachef.controllers.api.ApiBaseController;
import academy.belhard.pizzachef.dao.AbstractDAO;
import academy.belhard.pizzachef.dao.ClientDAO;
import academy.belhard.pizzachef.jsonobjects.ClientConvert;
import academy.belhard.pizzachef.model.entity.ClientEntity;
import academy.belhard.pizzachef.model.json.ClientJson;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ClientListController", urlPatterns = "/api/admin/getClientList")
public class ClientListController extends ApiBaseController{
    @Override
    protected void apiFunction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AbstractDAO<ClientEntity> clientDAO = new ClientDAO();
        List<ClientEntity> clientEntityList = clientDAO.getRecordList();

        List<ClientJson> clientJsonList = new ArrayList<>();
        for (ClientEntity clientEntity : clientEntityList) {
            ClientConvert clientRequest = new ClientConvert(clientEntity);
            clientJsonList.add(clientRequest.getClientJson());
        }

        Gson gson = new Gson();
        String jsonResponse = gson.toJson(clientJsonList);

        response.setContentType(ControllerConstants.CONTENT_TYPE_JSON);
        response.setCharacterEncoding(ControllerConstants.CHAR_ENCODING_UTF8);
        response.getWriter().write(jsonResponse);
    }
}
