package academy.belhard.pizzachef.model.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "orders")
@SequenceGenerator(name = "OrderIdGenerator", initialValue = 0, allocationSize = 1, sequenceName = "OrderIdGenerator")
public class OrderEntity {
    @Id
    @GeneratedValue(generator = "OrderIdGenerator", strategy = GenerationType.SEQUENCE)
    @Column(name = "order_id")
    private long id;
    @Column(name = "create_date_time")
    private Date createDateTime;
    @Column(name = "finished")
    private boolean finished;
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST,
                                                  CascadeType.REFRESH,
                                                  CascadeType.DETACH,
                                                  CascadeType.MERGE})
    @JoinColumn(name = "client_id", referencedColumnName = "client_id")
    private ClientEntity client;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "order")
    private List<PizzaEntity> listOfPizzas;

    public OrderEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(Date createDateTime) {
        this.createDateTime = createDateTime;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public ClientEntity getClient() {
        return client;
    }

    public void setClient(ClientEntity client) {
        this.client = client;
    }

    public List<PizzaEntity> getListOfPizzas() {
        return listOfPizzas;
    }

    public void setListOfPizzas(List<PizzaEntity> listOfPizzas) {
        this.listOfPizzas = listOfPizzas;
    }
}
