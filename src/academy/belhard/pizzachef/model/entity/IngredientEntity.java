package academy.belhard.pizzachef.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "ingredients")
@SequenceGenerator(name = "IngredientIdGenerator", initialValue = 0, allocationSize = 1, sequenceName = "IngredientIdGenerator")
public class IngredientEntity {
    @Id
    @GeneratedValue(generator = "IngredientIdGenerator", strategy = GenerationType.SEQUENCE)
    @Column(name = "ingredient_id")
    private long id;
    @Column(name = "name", nullable = false)
    private String name;
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST,
                                                  CascadeType.REFRESH,
                                                  CascadeType.DETACH,
                                                  CascadeType.MERGE})
    @JoinColumn(name = "category_id", referencedColumnName = "category_id")
    private CategoryEntity category;

    public IngredientEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }
}
