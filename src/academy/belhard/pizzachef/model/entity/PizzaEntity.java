package academy.belhard.pizzachef.model.entity;

import javax.persistence.*;
import java.util.Map;

@Entity
@Table(name = "pizzas")
@SequenceGenerator(name = "PizzaIdGenerator", initialValue = 0, allocationSize = 1, sequenceName = "PizzaIdGenerator")
public class PizzaEntity {
    @Id
    @GeneratedValue(generator = "PizzaIdGenerator", strategy = GenerationType.SEQUENCE)
    @Column(name = "pizza_id")
    private long id;
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST,
                                                 CascadeType.REFRESH,
                                                 CascadeType.DETACH,
                                                 CascadeType.MERGE})
    @JoinColumn(name = "pizza_size_id", referencedColumnName = "pizza_size_id")
    private PizzaSizeEntity pizzaSize;
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST,
                                                 CascadeType.REFRESH,
                                                 CascadeType.DETACH,
                                                 CascadeType.MERGE})
    @JoinColumn(name = "pizza_type_id", referencedColumnName = "pizza_type_id")
    private PizzaTypeEntity pizzaType;
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST,
                                                   CascadeType.REFRESH,
                                                   CascadeType.DETACH,
                                                   CascadeType.MERGE})
    @JoinTable(name = "map_pizzas_ingredients", joinColumns = @JoinColumn(name = "pizza_id"))
    @MapKeyJoinColumn(name = "layer")
    private Map<Byte, IngredientEntity> ingredientMap;
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST,
                                                  CascadeType.REFRESH,
                                                  CascadeType.DETACH,
                                                  CascadeType.MERGE})
    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    private OrderEntity order;

    public PizzaEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PizzaSizeEntity getPizzaSize() {
        return pizzaSize;
    }

    public void setPizzaSize(PizzaSizeEntity pizzaSize) {
        this.pizzaSize = pizzaSize;
    }

    public PizzaTypeEntity getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(PizzaTypeEntity pizzaType) {
        this.pizzaType = pizzaType;
    }

    public OrderEntity getOrder() {
        return order;
    }

    public void setOrder(OrderEntity order) {
        this.order = order;
    }

    public Map<Byte, IngredientEntity> getIngredientMap() {
        return ingredientMap;
    }

    public void setIngredientMap(Map<Byte, IngredientEntity> ingredientMap) {
        this.ingredientMap = ingredientMap;
    }
}
