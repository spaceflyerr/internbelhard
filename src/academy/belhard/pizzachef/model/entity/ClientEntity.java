package academy.belhard.pizzachef.model.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "clients")
@SequenceGenerator(name = "ClientIdGenerator", initialValue = 0, allocationSize = 1, sequenceName = "ClientIdGenerator")
public class ClientEntity {
    @Id
    @GeneratedValue(generator = "ClientIdGenerator", strategy = GenerationType.SEQUENCE)
    @Column(name = "client_id")
    private long id;
    @Column(name = "name", nullable = false, unique = true)
    private String name;
    @Column(name = "auth_key", nullable = false)
    private String authKey;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "contact_id", referencedColumnName = "contact_id")
    private ContactEntity contact;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "client")
    private List<OrderEntity> listOfOrders;

    public ClientEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ContactEntity getContact() {
        return contact;
    }

    public void setContact(ContactEntity contact) {
        this.contact = contact;
    }

    public List<OrderEntity> getListOfOrders() {
        return listOfOrders;
    }

    public void setListOfOrders(List<OrderEntity> listOfOrders) {
        this.listOfOrders = listOfOrders;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }
}
