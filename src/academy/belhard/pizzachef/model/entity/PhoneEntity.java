package academy.belhard.pizzachef.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "phones")
@SequenceGenerator(name = "PhoneIdGenerator", initialValue = 0, allocationSize = 1, sequenceName = "PhoneIdGenerator")
public class PhoneEntity {
    @Id
    @GeneratedValue(generator = "PhoneIdGenerator", strategy = GenerationType.SEQUENCE)
    @Column(name = "phone_id")
    private long id;
    @Column(name = "phone")
    private String phone;
    @Column(name = "department", columnDefinition = "TEXT")
    private String department;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "contact_id", referencedColumnName = "contact_id")
    private ContactEntity contact;

    public PhoneEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public ContactEntity getContact() {
        return contact;
    }

    public void setContact(ContactEntity contact) {
        this.contact = contact;
    }
}
