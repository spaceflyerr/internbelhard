package academy.belhard.pizzachef.model.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "categoryes")
@SequenceGenerator(name = "CategoryIdGenerator", initialValue = 0, allocationSize = 1, sequenceName = "CategoryIdGenerator")
public class CategoryEntity {
    @Id
    @GeneratedValue(generator = "CategoryIdGenerator", strategy = GenerationType.SEQUENCE)
    @Column(name = "category_id")
    private long id;
    @Column(name = "name", nullable = false)
    private String name;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "category")
    private List<IngredientEntity> listOfIngredients;

    public CategoryEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<IngredientEntity> getListOfIngredients() {
        return listOfIngredients;
    }

    public void setListOfIngredients(List<IngredientEntity> listOfIngredients) {
        this.listOfIngredients = listOfIngredients;
    }
}
