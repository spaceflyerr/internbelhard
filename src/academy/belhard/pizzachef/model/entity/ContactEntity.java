package academy.belhard.pizzachef.model.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "contacts")
@SequenceGenerator(name = "ContactIdGenerator", initialValue = 0, allocationSize = 1, sequenceName = "ContactIdGenerator0")
public class ContactEntity {
    @Id
    @GeneratedValue(generator = "ContactIdGenerator", strategy = GenerationType.SEQUENCE)
    @Column(name = "contact_id")
    private long id;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private AddressEntity address;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "contact")
    private List<PhoneEntity> phoneList;

    public ContactEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public AddressEntity getAddress() {
        return address;
    }

    public void setAddress(AddressEntity address) {
        this.address = address;
    }

    public List<PhoneEntity> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<PhoneEntity> phoneList) {
        this.phoneList = phoneList;
    }
}
