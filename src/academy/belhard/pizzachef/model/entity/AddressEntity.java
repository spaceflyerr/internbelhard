package academy.belhard.pizzachef.model.entity;

import academy.belhard.pizzachef.model.json.AddressJson;

import javax.persistence.*;

@Entity
@Table(name = "address")
@SequenceGenerator(name = "AddressIdGenerator", initialValue = 0, allocationSize = 1, sequenceName = "AddressIdGenerator")
public class AddressEntity {
    @Id
    @GeneratedValue(generator = "AddressIdGenerator", strategy = GenerationType.SEQUENCE)
    @Column(name = "address_id")
    private long id;
    @Column(name = "zip", nullable = false)
    private String zip;
    @Column(name = "country", nullable = false)
    private String country;
    @Column(name = "state", nullable = false)
    private String state;
    @Column(name = "city", nullable = false)
    private String city;
    @Column(name = "street", nullable = false)
    private String street;
    @Column(name = "building", nullable = false)
    private String building;
    @Column(name = "office", nullable = false)
    private String office;

    public AddressEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }
}
