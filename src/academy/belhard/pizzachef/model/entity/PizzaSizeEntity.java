package academy.belhard.pizzachef.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "pizza_sizes")
@SequenceGenerator(name = "PizzaSizeIdGenerator", initialValue = 0, allocationSize = 1, sequenceName = "PizzaSizeIdGenerator")
public class PizzaSizeEntity {
    @Id
    @GeneratedValue(generator = "PizzaSizeIdGenerator", strategy = GenerationType.SEQUENCE)
    @Column(name = "pizza_size_id")
    private long id;
    @Column(name = "diameter", columnDefinition = "INT(2)")
    private int diameter;
    @Column(name = "name", nullable = false)
    private String name;

    public PizzaSizeEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
