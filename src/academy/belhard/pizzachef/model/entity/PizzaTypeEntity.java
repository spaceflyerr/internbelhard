package academy.belhard.pizzachef.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "pizza_types")
@SequenceGenerator(name = "PizzaTypeIdGenerator", initialValue = 0, allocationSize = 1, sequenceName = "PizzaTypeIdGenerator")
public class PizzaTypeEntity {
    @Id
    @GeneratedValue(generator = "PizzaTypeIdGenerator", strategy = GenerationType.SEQUENCE)
    @Column(name = "pizza_type_id")
    private long id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    public PizzaTypeEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
