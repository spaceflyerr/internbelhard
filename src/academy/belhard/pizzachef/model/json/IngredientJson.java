package academy.belhard.pizzachef.model.json;

import academy.belhard.pizzachef.model.entity.CategoryEntity;
import academy.belhard.pizzachef.model.entity.IngredientEntity;
import com.google.gson.annotations.SerializedName;

public class IngredientJson {
    @SerializedName("name")
    private String name;
    @SerializedName("category")
    private CategoryJson category;

    public IngredientJson() {
    }

    public IngredientJson(IngredientEntity ingredient) {
        setName(ingredient.getName());
        copyCategoryFromModelToJson(ingredient.getCategory());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CategoryJson getCategory() {
        return category;
    }

    public void setCategory(CategoryJson category) {
        this.category = category;
    }

    private void copyCategoryFromModelToJson(CategoryEntity category) {
        this.category = new CategoryJson(category);
    }

    @Override
    public IngredientJson clone() {
        IngredientJson ingredientJson = new IngredientJson();
        ingredientJson.setName(name);
        ingredientJson.setCategory(category);
        return ingredientJson;
    }
}
