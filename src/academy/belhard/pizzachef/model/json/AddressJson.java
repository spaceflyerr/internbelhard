package academy.belhard.pizzachef.model.json;

import academy.belhard.pizzachef.model.entity.AddressEntity;
import com.google.gson.annotations.SerializedName;

public class AddressJson {
    @SerializedName("zip")
    private String zip;
    @SerializedName("country")
    private String country;
    @SerializedName("state")
    private String state;
    @SerializedName("city")
    private String city;
    @SerializedName("street")
    private String street;
    @SerializedName("building")
    private String building;
    @SerializedName("office")
    private String office;

    public AddressJson() {
    }

    public AddressJson(AddressEntity address) {
        setZip(address.getZip());
        setCountry(address.getCountry());
        setState(address.getState());
        setCity(address.getCity());
        setStreet(address.getStreet());
        setBuilding(address.getBuilding());
        setOffice(address.getOffice());
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    @Override
    public AddressJson clone() {
        AddressJson addressJson = new AddressJson();
        addressJson.setZip(zip);
        addressJson.setCountry(country);
        addressJson.setState(state);
        addressJson.setCity(city);
        addressJson.setStreet(street);
        addressJson.setBuilding(building);
        addressJson.setOffice(office);
        return  addressJson;
    }
}
