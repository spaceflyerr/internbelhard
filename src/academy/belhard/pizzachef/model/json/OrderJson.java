package academy.belhard.pizzachef.model.json;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class OrderJson {
    @SerializedName("id")
    private long id;
    @SerializedName("createDateTime")
    private Date createDateTime;
    @SerializedName("finished")
    private boolean finished;
    @SerializedName("pizzaList")
    private List<PizzaJson> pizzaList;

    public OrderJson() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(Date createDateTime) {
        this.createDateTime = createDateTime;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public List<PizzaJson> getPizzaList() {
        return pizzaList;
    }

    public void setPizzaList(List<PizzaJson> pizzaList) {
        this.pizzaList = pizzaList;
    }

    @Override
    public OrderJson clone() {
        OrderJson orderJson = new OrderJson();
        orderJson.setId(id);
        orderJson.setCreateDateTime(createDateTime);
        orderJson.setFinished(finished);
        orderJson.setPizzaList(pizzaList);
        return orderJson;
    }
}
