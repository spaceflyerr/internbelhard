package academy.belhard.pizzachef.model.json;

import academy.belhard.pizzachef.model.entity.PizzaSizeEntity;
import com.google.gson.annotations.SerializedName;

public class PizzaSizeJson {
    @SerializedName("diameter")
    private int diameter;
    @SerializedName("name")
    private String name;

    public PizzaSizeJson() {
    }

    public PizzaSizeJson(PizzaSizeEntity pizzaSize) {
        setName(pizzaSize.getName());
        setDiameter(pizzaSize.getDiameter());
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public PizzaSizeJson clone() {
        PizzaSizeJson pizzaSizeJson = new PizzaSizeJson();
        pizzaSizeJson.setDiameter(diameter);
        pizzaSizeJson.setName(name);
        return pizzaSizeJson;
    }
}
