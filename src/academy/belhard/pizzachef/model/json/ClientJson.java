package academy.belhard.pizzachef.model.json;

import academy.belhard.pizzachef.model.entity.ClientEntity;
import academy.belhard.pizzachef.model.entity.PhoneEntity;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ClientJson {
    @SerializedName("id")
    private long id;
    @SerializedName("name")
    private String name;
    @SerializedName("auth_key")
    private String authKey;
    @SerializedName("address")
    private AddressJson address;
    @SerializedName("phoneList")
    private List<PhoneJson> phoneList;

    public ClientJson() {
    }

    public ClientJson(ClientEntity client) {
        setId(client.getId());
        setName(client.getName());
        setAuthKey(client.getAuthKey());
        setAddress(new AddressJson(client.getContact().getAddress()));
        copyPhoneListFromModelToJson(client.getContact().getPhoneList());
    }

    private void copyPhoneListFromModelToJson(List<PhoneEntity> phoneList) {
        this.phoneList = new ArrayList<PhoneJson>();
        for (PhoneEntity phone : phoneList) {
            this.phoneList.add(new PhoneJson(phone));
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public AddressJson getAddress() {
        return address;
    }

    public void setAddress(AddressJson address) {
        this.address = address;
    }

    public List<PhoneJson> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<PhoneJson> phoneList) {
        this.phoneList = phoneList;
    }

    @Override
    public ClientJson clone() {
        ClientJson clientJson = new ClientJson();
        clientJson.setId(id);
        clientJson.setName(name);
        clientJson.setAuthKey(authKey);
        clientJson.setAddress(address);
        clientJson.setPhoneList(phoneList);
        return clientJson;
    }
}
