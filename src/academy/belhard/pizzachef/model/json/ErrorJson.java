package academy.belhard.pizzachef.model.json;

import academy.belhard.pizzachef.constants.ErrorConstants;
import com.google.gson.annotations.SerializedName;

public class ErrorJson {
    @SerializedName("error_code")
    private int error;
    @SerializedName("message")
    private String message;

    public ErrorJson() {
        this.error = ErrorConstants.ERROR_CODE_SUCCESSFUL;
        this.message = ErrorConstants.MESSAGE_SUCCESSFUL;
    }

    public ErrorJson(int error, String message) {
        this.error = error;
        this.message = message;
    }

    public int getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }
}
