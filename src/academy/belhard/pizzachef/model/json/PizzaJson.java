package academy.belhard.pizzachef.model.json;


import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class PizzaJson {
    @SerializedName("pizzaSize")
    private PizzaSizeJson pizzaSize;
    @SerializedName("pizzaType")
    private PizzaTypeJson pizzaType;
    @SerializedName("ingredients")
    private Map<Byte, IngredientJson> ingredientMap;

    public PizzaSizeJson getPizzaSize() {
        return pizzaSize;
    }

    public void setPizzaSize(PizzaSizeJson pizzaSize) {
        this.pizzaSize = pizzaSize;
    }

    public PizzaTypeJson getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(PizzaTypeJson pizzaType) {
        this.pizzaType = pizzaType;
    }

    public Map<Byte, IngredientJson> getIngredientMap() {
        return ingredientMap;
    }

    public void setIngredientMap(Map<Byte, IngredientJson> ingredientMap) {
        this.ingredientMap = ingredientMap;
    }

    @Override
    public PizzaJson clone() {
        PizzaJson pizzaJson = new PizzaJson();
        pizzaJson.setPizzaSize(pizzaSize);
        pizzaJson.setPizzaType(pizzaType);
        pizzaJson.setIngredientMap(ingredientMap);
        return pizzaJson;
    }
}
