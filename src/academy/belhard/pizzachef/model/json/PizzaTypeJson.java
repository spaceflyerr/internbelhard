package academy.belhard.pizzachef.model.json;

import academy.belhard.pizzachef.model.entity.PizzaTypeEntity;
import com.google.gson.annotations.SerializedName;

public class PizzaTypeJson {
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;

    public PizzaTypeJson(PizzaTypeEntity pizzaType) {
        setName(pizzaType.getName());
        setDescription(pizzaType.getDescription());
    }

    public PizzaTypeJson() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public PizzaTypeJson clone() {
        PizzaTypeJson pizzaTypeJson = new PizzaTypeJson();
        pizzaTypeJson.setName(name);
        pizzaTypeJson.setDescription(description);
        return pizzaTypeJson;
    }
}
