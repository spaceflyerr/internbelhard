package academy.belhard.pizzachef.model.json;

import academy.belhard.pizzachef.model.entity.PhoneEntity;
import com.google.gson.annotations.SerializedName;

public class PhoneJson {
    @SerializedName("phone")
    private String phone;
    @SerializedName("department")
    private String department;

    public PhoneJson() {
    }

    public PhoneJson(PhoneEntity phone) {
        setPhone(phone.getPhone());
        setDepartment(phone.getDepartment());
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public PhoneJson clone() {
        PhoneJson phoneJson = new PhoneJson();
        phoneJson.setDepartment(department);
        phoneJson.setPhone(phone);
        return phoneJson;
    }
}
