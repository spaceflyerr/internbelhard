package academy.belhard.pizzachef.model.json;

import academy.belhard.pizzachef.model.entity.CategoryEntity;
import com.google.gson.annotations.SerializedName;

public class CategoryJson {
    @SerializedName("name")
    private String name;

    public CategoryJson() {
    }

    public CategoryJson(CategoryEntity category) {
        setName(category.getName());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public CategoryJson clone() {
        CategoryJson categoryJson = new CategoryJson();
        categoryJson.setName(name);
        return categoryJson;
    }
}
