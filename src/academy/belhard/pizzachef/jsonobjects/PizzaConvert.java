package academy.belhard.pizzachef.jsonobjects;

import academy.belhard.pizzachef.model.entity.IngredientEntity;
import academy.belhard.pizzachef.model.entity.PizzaEntity;
import academy.belhard.pizzachef.model.entity.PizzaSizeEntity;
import academy.belhard.pizzachef.model.entity.PizzaTypeEntity;
import academy.belhard.pizzachef.model.json.IngredientJson;
import academy.belhard.pizzachef.model.json.PizzaJson;
import academy.belhard.pizzachef.model.json.PizzaSizeJson;
import academy.belhard.pizzachef.model.json.PizzaTypeJson;

import java.util.HashMap;
import java.util.Map;

public class PizzaConvert {
    private PizzaJson pizzaJson;
    private PizzaEntity pizzaEntity;

    public PizzaConvert(PizzaJson pizzaJson) {
        this.pizzaJson = pizzaJson;
        this.pizzaEntity = new PizzaEntity();
        this.pizzaEntity.setPizzaSize(copyPizzaSizeToEntity(pizzaJson.getPizzaSize()));
        this.pizzaEntity.setPizzaType(copyPizzaTypeToEntity(pizzaJson.getPizzaType()));
        this.pizzaEntity.setIngredientMap(copyIngredientToEntityMap(pizzaJson.getIngredientMap()));
    }

    public PizzaConvert(PizzaEntity pizzaEntity) {
        this.pizzaEntity = pizzaEntity;
        this.pizzaJson = new PizzaJson();
        this.pizzaJson.setPizzaSize(copyPizzaSizeToJson(pizzaEntity.getPizzaSize()));
        this.pizzaJson.setPizzaType(copyPizzaTypeToJson(pizzaEntity.getPizzaType()));
        this.pizzaJson.setIngredientMap(copyIngredientToJsonMap(pizzaEntity.getIngredientMap()));
    }

    private Map<Byte,IngredientJson> copyIngredientToJsonMap(Map<Byte, IngredientEntity> ingredientEntityMap) {
        Map<Byte, IngredientJson> ingredientJsonMap = new HashMap<>();
        for (Map.Entry<Byte, IngredientEntity> entry : ingredientEntityMap.entrySet()) {
            IngredientConvert ingredientConvert = new IngredientConvert(entry.getValue());
            ingredientJsonMap.put(entry.getKey(), ingredientConvert.getIngredientJson());
        }
        return ingredientJsonMap;
    }

    private PizzaSizeJson copyPizzaSizeToJson(PizzaSizeEntity pizzaSize) {
        PizzaSizeConvert pizzaSizeConvert = new PizzaSizeConvert(pizzaSize);
        return pizzaSizeConvert.getPizzaSizeJson();
    }

    private PizzaTypeJson copyPizzaTypeToJson(PizzaTypeEntity pizzaType) {
        PizzaTypeConvert pizzaTypeConvert = new PizzaTypeConvert(pizzaType);
        return pizzaTypeConvert.getPizzaTypeJson();
    }

    private Map<Byte,IngredientEntity> copyIngredientToEntityMap(Map<Byte, IngredientJson> ingredientJsonMap) {
        Map<Byte, IngredientEntity> ingredientEntityMap = new HashMap<>();
        for (Map.Entry<Byte, IngredientJson> entry : ingredientJsonMap.entrySet()) {
            IngredientConvert ingredientConvert = new IngredientConvert(entry.getValue());
            ingredientEntityMap.put(entry.getKey(), ingredientConvert.getIngredientEntity());
        }
        return ingredientEntityMap;
    }

    private PizzaTypeEntity copyPizzaTypeToEntity(PizzaTypeJson pizzaType) {
        PizzaTypeConvert pizzaTypeConvert = new PizzaTypeConvert(pizzaType);
        return pizzaTypeConvert.getPizzaTypeEntity();
    }

    private PizzaSizeEntity copyPizzaSizeToEntity(PizzaSizeJson pizzaSize) {
        PizzaSizeConvert pizzaSizeConvert = new PizzaSizeConvert(pizzaSize);
        return pizzaSizeConvert.getPizzaSizeEntity();
    }


    public PizzaEntity getPizzaEntity() {
        return pizzaEntity;
    }

    public PizzaJson getPizzaJson() {
        return pizzaJson.clone();
    }
}
