package academy.belhard.pizzachef.jsonobjects;

import academy.belhard.pizzachef.model.entity.AddressEntity;
import academy.belhard.pizzachef.model.entity.ClientEntity;
import academy.belhard.pizzachef.model.entity.ContactEntity;
import academy.belhard.pizzachef.model.entity.PhoneEntity;
import academy.belhard.pizzachef.model.json.AddressJson;
import academy.belhard.pizzachef.model.json.ClientJson;
import academy.belhard.pizzachef.model.json.PhoneJson;

import java.util.ArrayList;
import java.util.List;

public class ClientConvert {
    private ClientJson clientJson;
    private ClientEntity clientEntity;

    public ClientConvert(ClientJson clientJson) {
        this.clientJson = clientJson;
        this.clientEntity = new ClientEntity();
        this.clientEntity.setId(clientJson.getId());
        this.clientEntity.setName(clientJson.getName());
        this.clientEntity.setAuthKey(clientJson.getAuthKey());
        this.clientEntity.setContact(getContactFrom(clientJson.getAddress(),clientJson.getPhoneList()));
    }

    public ClientConvert(ClientEntity clientEntity) {
        this.clientEntity = clientEntity;
        this.clientJson = new ClientJson();
        this.clientJson.setId(clientEntity.getId());
        this.clientJson.setName(clientEntity.getName());
        this.clientJson.setAuthKey(clientEntity.getAuthKey());
        this.clientJson.setAddress(getAddressFrom(clientEntity.getContact().getAddress()));
        this.clientJson.setPhoneList(getPhoneListFrom(clientEntity.getContact().getPhoneList()));
    }

    private ContactEntity getContactFrom(AddressJson address, List<PhoneJson> phoneList) {
        ContactEntity contactEntity = new ContactEntity();
        AddressConvert addressConvert = new AddressConvert(address);
        contactEntity.setAddress(addressConvert.getAddressEntity());
        List<PhoneEntity> phoneEntityList = new ArrayList<>();
        for (PhoneJson phoneJson : phoneList) {
            PhoneConvert phoneConvert = new PhoneConvert(phoneJson);
            PhoneEntity phone = phoneConvert.getPhoneEntity();
            phone.setContact(contactEntity);
            phoneEntityList.add(phone);
        }
        contactEntity.setPhoneList(phoneEntityList);
        return contactEntity;
    }

    private List<PhoneJson> getPhoneListFrom(List<PhoneEntity> phoneList) {
        List<PhoneJson> phoneJsonList = new ArrayList<>();
        for (PhoneEntity phoneEntity : phoneList) {
            PhoneConvert phoneConvert = new PhoneConvert(phoneEntity);
            phoneJsonList.add(phoneConvert.getPhoneJson());
        }
        return phoneJsonList;
    }

    private AddressJson getAddressFrom(AddressEntity address) {
        AddressConvert addressConvert = new AddressConvert(address);
        return addressConvert.getAddressJson();
    }

    public ClientJson getClientJson() {
        return clientJson.clone();
    }

    public ClientEntity getClientEntity() {
        return clientEntity;
    }
}
