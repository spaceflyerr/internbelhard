package academy.belhard.pizzachef.jsonobjects;

import academy.belhard.pizzachef.model.entity.PhoneEntity;
import academy.belhard.pizzachef.model.json.PhoneJson;

public class PhoneConvert {
    private PhoneJson phoneJson;
    private PhoneEntity phoneEntity;

    public PhoneConvert(PhoneJson phoneJson) {
        this.phoneJson = phoneJson;
        this.phoneEntity = new PhoneEntity();
        this.phoneEntity.setPhone(phoneJson.getPhone());
        this.phoneEntity.setDepartment(phoneJson.getDepartment());
    }

    public PhoneConvert(PhoneEntity phoneEntity) {
        this.phoneEntity = phoneEntity;
        this.phoneJson = new PhoneJson();
        this.phoneJson.setPhone(phoneEntity.getPhone());
        this.phoneJson.setDepartment(phoneEntity.getDepartment());
    }

    public PhoneEntity getPhoneEntity() {
        return phoneEntity;
    }

    public PhoneJson getPhoneJson() {
        return phoneJson.clone();
    }
}
