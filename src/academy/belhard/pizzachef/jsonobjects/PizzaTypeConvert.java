package academy.belhard.pizzachef.jsonobjects;


import academy.belhard.pizzachef.model.entity.PizzaTypeEntity;
import academy.belhard.pizzachef.model.json.PizzaTypeJson;

public class PizzaTypeConvert {
    private PizzaTypeJson pizzaTypeJson;
    private PizzaTypeEntity pizzaTypeEntity;

    public PizzaTypeConvert(PizzaTypeEntity pizzaSizeEntity) {
        this.pizzaTypeEntity = pizzaSizeEntity;
        this.pizzaTypeJson = new PizzaTypeJson();
        this.pizzaTypeJson.setDescription(pizzaSizeEntity.getDescription());
        this.pizzaTypeJson.setName(pizzaSizeEntity.getName());
    }

    public PizzaTypeConvert(PizzaTypeJson pizzaSizeJson) {
        this.pizzaTypeJson = pizzaSizeJson;
        this.pizzaTypeEntity = new PizzaTypeEntity();
        this.pizzaTypeEntity.setDescription(pizzaSizeJson.getDescription());
        this.pizzaTypeEntity.setName(pizzaSizeJson.getName());
    }

    public PizzaTypeJson getPizzaTypeJson() {
        return pizzaTypeJson.clone();
    }

    public PizzaTypeEntity getPizzaTypeEntity() {
        return pizzaTypeEntity;
    }
}
