package academy.belhard.pizzachef.jsonobjects;

import academy.belhard.pizzachef.model.entity.OrderEntity;
import academy.belhard.pizzachef.model.entity.PizzaEntity;
import academy.belhard.pizzachef.model.json.OrderJson;
import academy.belhard.pizzachef.model.json.PizzaJson;

import java.util.ArrayList;
import java.util.List;

public class OrderConvert {
    private OrderEntity orderEntity;
    private OrderJson orderJson;

    public OrderConvert(OrderEntity orderEntity) {
        this.orderEntity = orderEntity;
        this.orderJson = new OrderJson();
        this.orderJson.setId(orderEntity.getId());
        this.orderJson.setCreateDateTime(orderEntity.getCreateDateTime());
        this.orderJson.setFinished(orderEntity.isFinished());
        this.orderJson.setPizzaList(copyPizzaToJsonList(orderEntity.getListOfPizzas()));

    }

    public OrderConvert(OrderJson orderJson) {
        this.orderJson = orderJson;
        this.orderEntity = new OrderEntity();
        this.orderEntity.setId(orderJson.getId());
        this.orderEntity.setCreateDateTime(orderJson.getCreateDateTime());
        this.orderEntity.setFinished(orderJson.isFinished());
        this.orderEntity.setListOfPizzas(copyPizzaToEntityList(orderJson.getPizzaList()));
    }

    public OrderEntity getOrderEntity() {
        return orderEntity;
    }

    public OrderJson getOrderJson() {
        return orderJson.clone();
    }

    private List<PizzaEntity> copyPizzaToEntityList(List<PizzaJson> pizzaList) {
        List<PizzaEntity> pizzaEntityList = new ArrayList<>();
        for (PizzaJson pizzaJson: pizzaList) {
            PizzaConvert pizza = new PizzaConvert(pizzaJson);
            PizzaEntity pizzaEntity = pizza.getPizzaEntity();
            pizzaEntity.setOrder(this.orderEntity);
            pizzaEntityList.add(pizzaEntity);
        }
        return pizzaEntityList;
    }

    private List<PizzaJson> copyPizzaToJsonList(List<PizzaEntity> listOfPizzas) {
        List<PizzaJson> pizzaJsonList = new ArrayList<>();
        for (PizzaEntity pizzaEntity : listOfPizzas) {
            PizzaConvert pizza = new PizzaConvert(pizzaEntity);
            pizzaJsonList.add(pizza.getPizzaJson());
        }
        return pizzaJsonList;
    }
}
