package academy.belhard.pizzachef.jsonobjects;

import academy.belhard.pizzachef.model.entity.CategoryEntity;
import academy.belhard.pizzachef.model.json.CategoryJson;

public class CategoryConvert {
    private CategoryJson categoryJson;
    private CategoryEntity categoryEntity;

    public CategoryConvert(CategoryEntity category) {
        this.categoryEntity = category;
        this.categoryJson = new CategoryJson();
        this.categoryJson.setName(category.getName());
    }

    public CategoryConvert(CategoryJson category) {
        this.categoryJson = category;
        this.categoryEntity = new CategoryEntity();
        this.categoryEntity.setName(category.getName());
    }

    public CategoryJson getCategoryJson() {
        return categoryJson.clone();
    }

    public CategoryEntity getCategoryEntity() {
        return categoryEntity;
    }
}
