package academy.belhard.pizzachef.jsonobjects;

import academy.belhard.pizzachef.model.entity.PizzaSizeEntity;
import academy.belhard.pizzachef.model.json.PizzaSizeJson;

public class PizzaSizeConvert {
    private PizzaSizeJson pizzaSizeJson;
    private PizzaSizeEntity pizzaSizeEntity;

    public PizzaSizeConvert(PizzaSizeEntity pizzaSizeEntity) {
        this.pizzaSizeEntity = pizzaSizeEntity;
        this.pizzaSizeJson = new PizzaSizeJson();
        this.pizzaSizeJson.setDiameter(pizzaSizeEntity.getDiameter());
        this.pizzaSizeJson.setName(pizzaSizeEntity.getName());
    }

    public PizzaSizeConvert(PizzaSizeJson pizzaSizeJson) {
        this.pizzaSizeJson = pizzaSizeJson;
        this.pizzaSizeEntity = new PizzaSizeEntity();
        this.pizzaSizeEntity.setDiameter(pizzaSizeJson.getDiameter());
        this.pizzaSizeEntity.setName(pizzaSizeJson.getName());
    }

    public PizzaSizeJson getPizzaSizeJson() {
        return pizzaSizeJson.clone();
    }

    public PizzaSizeEntity getPizzaSizeEntity() {
        return pizzaSizeEntity;
    }
}
