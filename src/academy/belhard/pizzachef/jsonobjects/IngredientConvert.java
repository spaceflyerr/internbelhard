package academy.belhard.pizzachef.jsonobjects;

import academy.belhard.pizzachef.model.entity.CategoryEntity;
import academy.belhard.pizzachef.model.entity.IngredientEntity;
import academy.belhard.pizzachef.model.json.CategoryJson;
import academy.belhard.pizzachef.model.json.IngredientJson;

public class IngredientConvert {
    private IngredientEntity ingredientEntity;
    private IngredientJson ingredientJson;

    public IngredientConvert(IngredientEntity ingredientEntity) {
        this.ingredientEntity = ingredientEntity;
        this.ingredientJson = new IngredientJson();
        this.ingredientJson.setName(ingredientEntity.getName());
        this.ingredientJson.setCategory(copyCategoryToJson(ingredientEntity.getCategory()));
    }

    private CategoryJson copyCategoryToJson(CategoryEntity category) {
        CategoryConvert categoryConvetr = new CategoryConvert(category);
        return categoryConvetr.getCategoryJson();
    }

    public IngredientConvert(IngredientJson ingredientJson) {
        this.ingredientJson = ingredientJson;
        this.ingredientEntity = new IngredientEntity();
        this.ingredientEntity.setName(ingredientJson.getName());
        this.ingredientEntity.setCategory(copyCategoryToEntity(ingredientJson.getCategory()));
    }

    private CategoryEntity copyCategoryToEntity(CategoryJson category) {
        CategoryConvert categoryConvetr = new CategoryConvert(category);
        return categoryConvetr.getCategoryEntity();
    }

    public IngredientJson getIngredientJson() {
        return ingredientJson.clone();
    }

    public IngredientEntity getIngredientEntity() {
        return ingredientEntity;
    }
}
