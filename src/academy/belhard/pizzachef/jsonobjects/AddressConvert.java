package academy.belhard.pizzachef.jsonobjects;

import academy.belhard.pizzachef.model.entity.AddressEntity;
import academy.belhard.pizzachef.model.json.AddressJson;

public class AddressConvert {
    private AddressJson addressJson;
    private AddressEntity addressEntity;

    public AddressConvert(AddressJson address) {
        this.addressJson = address;
        this.addressEntity = new AddressEntity();
        this.addressEntity.setZip(address.getZip());
        this.addressEntity.setCountry(address.getCountry());
        this.addressEntity.setState(address.getState());
        this.addressEntity.setCity(address.getCity());
        this.addressEntity.setStreet(address.getStreet());
        this.addressEntity.setBuilding(address.getBuilding());
        this.addressEntity.setOffice(address.getOffice());
    }

    public AddressConvert(AddressEntity address) {
        this.addressEntity = address;
        this.addressJson = new AddressJson();
        this.addressJson.setZip(address.getZip());
        this.addressJson.setCountry(address.getCountry());
        this.addressJson.setState(address.getState());
        this.addressJson.setCity(address.getCity());
        this.addressJson.setStreet(address.getStreet());
        this.addressJson.setBuilding(address.getBuilding());
        this.addressJson.setOffice(address.getOffice());
    }


    public AddressEntity getAddressEntity() {
        return addressEntity;
    }

    public AddressJson getAddressJson() {
        return addressJson.clone();
    }
}
