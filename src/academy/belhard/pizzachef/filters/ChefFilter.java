package academy.belhard.pizzachef.filters;

import academy.belhard.pizzachef.constants.ControllerConstants;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "ChefFilter", urlPatterns = "/api/chef/*")
public class ChefFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        String chefId = request.getParameter(ControllerConstants.PARAMETER_NAME_IN_REQUEST_FOR_ALL_ORDERS_FOR_CHEF);
        if (chefId == null || !chefId.equals(ControllerConstants.PARAMETER_VALUE_IN_REQUEST_FOR_ALL_ORDERS_FOR_CHEF)) {
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            httpResponse.sendRedirect(ControllerConstants.ROOT_DIR_SITE);
            return;
        }
        chain.doFilter(request, response);
    }

}
