package academy.belhard.pizzachef.constants;

public class HibernateConstants {
	public static final String PERSISTANCE_NAME = "PizzaChef";
	public static final String DB_HOST = "localhost";
	public static final String DB_USER_NAME = "pizzachef";
	public static final String DB_USER_PASSWORD = "root";
	public static final String DB_NAME = "pizzachef";
	public static final String DB_URL = "jdbc:mysql://127.0.0.1:3306/pizzachef?characterEncoding=utf-8";

}
