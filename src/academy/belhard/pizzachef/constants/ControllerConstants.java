package academy.belhard.pizzachef.constants;

public class ControllerConstants {
    public static final String VIEW_NAME_TEMPLATE = "WEB-INF/views/%sView.jsp";
    public static final String CONTENT_TYPE_JSON = "application/json";
    public static final String CHAR_ENCODING_UTF8 = "UTF-8";

    public static final String ROOT_DIR_SITE = "/";
    public static final String PARAMETER_NAME_IN_REQUEST_FOR_ALL_ORDERS = "clientId";
    public static final String PARAMETER_NAME_IN_REQUEST_FOR_ALL_ORDERS_FOR_CHEF = "chefId";
    public static final String PARAMETER_VALUE_IN_REQUEST_FOR_ALL_ORDERS_FOR_CHEF = "i'm a chef in the PizzaChef";

    public static final String CLIENT_JSON_NAME_IN_REQUEST = "client_json";
    public static final String INGREDIENT_JSON_NAME_IN_REQUEST = "ingredient_json";
    public static final String PIZZA_SIZE_JSON_NAME_IN_REQUEST = "pizza_size_json";
    public static final String PIZZA_TYPE_JSON_NAME_IN_REQUEST = "pizza_type_json";


}
