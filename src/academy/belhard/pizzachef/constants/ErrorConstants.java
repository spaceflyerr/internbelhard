package academy.belhard.pizzachef.constants;

public class ErrorConstants {
    public static final int ERROR_CODE_SUCCESSFUL = 0;
    public static final String MESSAGE_SUCCESSFUL = "Successful";

    public static final int CODE_OF_ERROR_ADD_ORDER = 10;
    public static final String MESSAGE_OF_ERROR_ADD_ORDER = "This order didn't add to database.";

    public static final int CODE_OF_ERROR_ADD_CLIENT = 20;
    public static final String MESSAGE_OF_ERROR_ADD_CLIENT = "This client didn't add to database.";

    public static final int CODE_OF_ERROR_ADD_INGREDIENT = 30;
    public static final String MESSAGE_OF_ERROR_ADD_INGREDIENT = "This ingredient didn't add to database.";

    public static final int CODE_OF_ERROR_ADD_PIZZA_SIZE = 40;
    public static final String MESSAGE_OF_ERROR_ADD_PIZZA_SIZE = "This pizza size didn't add to database.";

    public static final int CODE_OF_ERROR_ADD_PIZZA_TYPE = 50;
    public static final String MESSAGE_OF_ERROR_ADD_PIZZA_TYPE = "This pizza type didn't add to database.";
}
