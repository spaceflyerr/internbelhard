package academy.belhard.pizzachef.utils;

import academy.belhard.pizzachef.constants.HibernateConstants;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class HibernateUtils {
    private static final EntityManager entityManager = Persistence.createEntityManagerFactory(HibernateConstants.PERSISTANCE_NAME).createEntityManager();

    public static EntityManager getEntityManager(){
        return entityManager;
    }
}
