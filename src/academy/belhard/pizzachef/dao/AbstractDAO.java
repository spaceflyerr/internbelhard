package academy.belhard.pizzachef.dao;

import academy.belhard.pizzachef.utils.HibernateUtils;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Map;

abstract public class AbstractDAO<T> {

    public T addRecord(T object) {
        EntityManager em = HibernateUtils.getEntityManager();
        em.getTransaction().begin();

        try {
            em.merge(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
        return object;
    }

    abstract public List<T> getRecordList(Map<String, Object> findByMap);

    abstract public List<T> getRecordList();

    abstract public T getRecordById(long id);

    public T updateRecord(T object) {
        EntityManager em = HibernateUtils.getEntityManager();
        em.getTransaction().begin();

        try {
            em.merge(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
        return object;
    }

    public void deleteRecord(T object) {
        EntityManager em = HibernateUtils.getEntityManager();
        em.getTransaction().begin();

        try {
            em.remove(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }

}
