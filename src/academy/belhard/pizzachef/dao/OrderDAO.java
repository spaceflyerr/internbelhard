package academy.belhard.pizzachef.dao;

import academy.belhard.pizzachef.model.entity.OrderEntity;
import academy.belhard.pizzachef.utils.HibernateUtils;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static academy.belhard.pizzachef.constants.DAOConstants.PARAMETER_ID;

public class OrderDAO extends AbstractDAO<OrderEntity>{

    @Override
    public List<OrderEntity> getRecordList(Map<String, Object> findByMap) {
        EntityManager em = HibernateUtils.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<OrderEntity> cq = cb.createQuery(OrderEntity.class);
        Root<OrderEntity> from = cq.from(OrderEntity.class);
        cq.select(from);
        Predicate criteria = cb.conjunction();
        for (Map.Entry<String, Object> entry : findByMap.entrySet()) {
            Predicate p = cb.equal(from.get(entry.getKey()), entry.getValue());
            criteria = cb.and(criteria, p);
        }
        cq.where(criteria);
        List<OrderEntity> orderEntityList = em.createQuery(cq).getResultList();
        return orderEntityList;
    }

    @Override
    public List<OrderEntity> getRecordList() {
        return getRecordList(new HashMap());
    }

    @Override
    public OrderEntity getRecordById(long id) {
        EntityManager em = HibernateUtils.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<OrderEntity> cq = cb.createQuery(OrderEntity.class);
        Root<OrderEntity> from = cq.from(OrderEntity.class);
        cq.select(from);
        cq.where(cb.equal(from.get(PARAMETER_ID), id));
        OrderEntity orderEntity = em.createQuery(cq).getSingleResult();
        return orderEntity;
    }

}
