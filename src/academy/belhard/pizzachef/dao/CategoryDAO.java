package academy.belhard.pizzachef.dao;

import academy.belhard.pizzachef.model.entity.CategoryEntity;
import academy.belhard.pizzachef.utils.HibernateUtils;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static academy.belhard.pizzachef.constants.DAOConstants.PARAMETER_ID;

public class CategoryDAO extends AbstractDAO<CategoryEntity> {
    @Override
    public List<CategoryEntity> getRecordList(Map<String, Object> findByMap) {
        EntityManager em = HibernateUtils.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<CategoryEntity> cq = cb.createQuery(CategoryEntity.class);
        Root<CategoryEntity> from = cq.from(CategoryEntity.class);
        cq.select(from);
        Predicate criteria = cb.conjunction();
        for (Map.Entry<String, Object> entry : findByMap.entrySet()) {
            Predicate p = cb.equal(from.get(entry.getKey()), entry.getValue());
            criteria = cb.and(criteria, p);
        }
        cq.where(criteria);
        List<CategoryEntity> categoryEntityList = em.createQuery(cq).getResultList();
        return categoryEntityList;
    }

    @Override
    public List<CategoryEntity> getRecordList() {
        return getRecordList(new HashMap());
    }

    @Override
    public CategoryEntity getRecordById(long id) {
        EntityManager em = HibernateUtils.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<CategoryEntity> cq = cb.createQuery(CategoryEntity.class);
        Root<CategoryEntity> from = cq.from(CategoryEntity.class);
        cq.select(from);
        cq.where(cb.equal(from.get(PARAMETER_ID), id));
        CategoryEntity categoryEntity = em.createQuery(cq).getSingleResult();
        em.getTransaction().commit();
        return categoryEntity;
    }
}
