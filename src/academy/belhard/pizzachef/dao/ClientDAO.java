package academy.belhard.pizzachef.dao;

import academy.belhard.pizzachef.model.entity.ClientEntity;
import academy.belhard.pizzachef.utils.HibernateUtils;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static academy.belhard.pizzachef.constants.DAOConstants.PARAMETER_ID;

public class ClientDAO extends AbstractDAO<ClientEntity> {

    @Override
    public List<ClientEntity> getRecordList(Map<String, Object> findByMap) {
        EntityManager em = HibernateUtils.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<ClientEntity> cq = cb.createQuery(ClientEntity.class);
        Root<ClientEntity> from = cq.from(ClientEntity.class);
        cq.select(from);
        Predicate criteria = cb.conjunction();
        for (Map.Entry<String, Object> entry : findByMap.entrySet()) {
            Predicate p = cb.equal(from.get(entry.getKey()), entry.getValue());
            criteria = cb.and(criteria, p);
        }
        cq.where(criteria);
        List<ClientEntity> clientEntityList = em.createQuery(cq).getResultList();
        return clientEntityList;
    }

    @Override
    public List<ClientEntity> getRecordList() {
        return getRecordList(new HashMap());
    }

    @Override
    public ClientEntity getRecordById(long id) {
        EntityManager em = HibernateUtils.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<ClientEntity> cq = cb.createQuery(ClientEntity.class);
        Root<ClientEntity> from = cq.from(ClientEntity.class);
        cq.select(from);
        cq.where(cb.equal(from.get(PARAMETER_ID), id));
        ClientEntity clientEntity = em.createQuery(cq).getSingleResult();
        return clientEntity;
    }

}
