package academy.belhard.pizzachef.dao;

import academy.belhard.pizzachef.model.entity.PizzaTypeEntity;
import academy.belhard.pizzachef.utils.HibernateUtils;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static academy.belhard.pizzachef.constants.DAOConstants.PARAMETER_ID;

public class PizzaTypeDAO extends AbstractDAO<PizzaTypeEntity> {
    @Override
    public List<PizzaTypeEntity> getRecordList(Map<String, Object> findByMap) {
        EntityManager em = HibernateUtils.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<PizzaTypeEntity> cq = cb.createQuery(PizzaTypeEntity.class);
        Root<PizzaTypeEntity> from = cq.from(PizzaTypeEntity.class);
        cq.select(from);
        Predicate criteria = cb.conjunction();
        for (Map.Entry<String, Object> entry : findByMap.entrySet()) {
            Predicate p = cb.equal(from.get(entry.getKey()), entry.getValue());
            criteria = cb.and(criteria, p);
        }
        cq.where(criteria);
        List<PizzaTypeEntity> pizzaTypeEntityList = em.createQuery(cq).getResultList();
        return pizzaTypeEntityList;
    }

    @Override
    public List<PizzaTypeEntity> getRecordList() {
        return getRecordList(new HashMap());
    }

    @Override
    public PizzaTypeEntity getRecordById(long id) {
        EntityManager em = HibernateUtils.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<PizzaTypeEntity> cq = cb.createQuery(PizzaTypeEntity.class);
        Root<PizzaTypeEntity> from = cq.from(PizzaTypeEntity.class);
        cq.select(from);
        cq.where(cb.equal(from.get(PARAMETER_ID), id));
        PizzaTypeEntity pizzaTypeEntity = em.createQuery(cq).getSingleResult();
        em.getTransaction().commit();
        return pizzaTypeEntity;
    }
}
