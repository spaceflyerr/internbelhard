package academy.belhard.pizzachef.test;

import academy.belhard.pizzachef.controllers.api.ApiBaseController;
import academy.belhard.pizzachef.model.entity.*;
import academy.belhard.pizzachef.utils.HibernateUtils;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@WebServlet(name = "RunTest", urlPatterns = "/addTestToBase")
public class RunTest extends ApiBaseController {
    @Override
    protected void apiFunction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        EntityManager em = HibernateUtils.getEntityManager();
        em.getTransaction().begin();

        CategoryEntity category_1 = new CategoryEntity();
        category_1.setName("Cheeses");

        CategoryEntity category_2 = new CategoryEntity();
        category_2.setName("Paprika");

        IngredientEntity ingredient_1 = new IngredientEntity();
        ingredient_1.setName("Mazzarella");
        ingredient_1.setCategory(category_1);

        IngredientEntity ingredient_2 = new IngredientEntity();
        ingredient_2.setName("Hohland");
        ingredient_2.setCategory(category_1);

        IngredientEntity ingredient_3 = new IngredientEntity();
        ingredient_3.setName("Chile");
        ingredient_3.setCategory(category_2);

        Map<Byte, IngredientEntity> mapIngredients_3 = new HashMap<>();
        mapIngredients_3.put((byte)1, ingredient_1);

        Map<Byte, IngredientEntity> mapIngredients_2 = new HashMap<>();
        mapIngredients_2.put((byte)1, ingredient_3);
        mapIngredients_2.put((byte)2, ingredient_2);

        Map<Byte, IngredientEntity> mapIngredients_1 = new HashMap<>();
        mapIngredients_1.put((byte)1, ingredient_3);
        mapIngredients_1.put((byte)2, ingredient_1);
        mapIngredients_1.put((byte)3, ingredient_2);

        PizzaSizeEntity size_1 = new PizzaSizeEntity();
        size_1.setDiameter(16);
        size_1.setName("small");

        PizzaTypeEntity type_1 = new PizzaTypeEntity();
        type_1.setName("single");
        type_1.setDescription("Only for you");

        PizzaSizeEntity size_2 = new PizzaSizeEntity();
        size_2.setDiameter(32);
        size_2.setName("medium");

        PizzaTypeEntity type_2 = new PizzaTypeEntity();
        type_2.setName("date");
        type_2.setDescription("And me and you");

        PizzaSizeEntity size_3 = new PizzaSizeEntity();
        size_3.setDiameter(48);
        size_3.setName("large");

        PizzaTypeEntity type_3 = new PizzaTypeEntity();
        type_3.setName("family");
        type_3.setDescription("For family");

        PizzaEntity pizza_1 = new PizzaEntity();
        pizza_1.setPizzaSize(size_1);
        pizza_1.setPizzaType(type_1);
        pizza_1.setIngredientMap(mapIngredients_1);

        PizzaEntity pizza_2 = new PizzaEntity();
        pizza_2.setPizzaSize(size_2);
        pizza_2.setPizzaType(type_2);
        pizza_2.setIngredientMap(mapIngredients_2);

        PizzaEntity pizza_3 = new PizzaEntity();
        pizza_3.setPizzaSize(size_3);
        pizza_3.setPizzaType(type_3);
        pizza_3.setIngredientMap(mapIngredients_3);

        List<PizzaEntity> pizzas = new ArrayList<>();
        pizzas.add(pizza_1);
        pizzas.add(pizza_2);
        pizzas.add(pizza_3);

        AddressEntity address = new AddressEntity();
        address.setCountry("Belarus");
        address.setState("Minsk region");
        address.setCity("Minsk");
        address.setStreet("Gogol");
        address.setBuilding("45b");
        address.setOffice("1");
        address.setZip("246000");

        PhoneEntity phone_1 = new PhoneEntity();
        phone_1.setPhone("+375 (117) 257 57 32");
        phone_1.setDepartment("CEO");

        PhoneEntity phone_2 = new PhoneEntity();
        phone_2.setPhone("+375 (117) 257 57 33");
        phone_2.setDepartment("accounting");

        List<PhoneEntity> phones = new ArrayList<>();
        phones.add(phone_1);
        phones.add(phone_2);

        ContactEntity contact = new ContactEntity();
        contact.setAddress(address);
        contact.setPhoneList(phones);

        phone_1.setContact(contact);
        phone_2.setContact(contact);

        ClientEntity client = new ClientEntity();
        client.setName("Papa Jones");
        client.setAuthKey("JHDSYUGB3RH3FN34F3V4R3490-DSF");
        client.setContact(contact);

        OrderEntity order = new OrderEntity();
        order.setListOfPizzas(pizzas);
        order.setClient(client);
        order.setFinished(false);
        order.setCreateDateTime(new Date());

        pizza_1.setOrder(order);
        pizza_2.setOrder(order);
        pizza_3.setOrder(order);

        try {
            em.persist(order);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }
}
