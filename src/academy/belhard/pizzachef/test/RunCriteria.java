package academy.belhard.pizzachef.test;

import academy.belhard.pizzachef.dao.AbstractDAO;
import academy.belhard.pizzachef.dao.ClientDAO;
import academy.belhard.pizzachef.dao.OrderDAO;
import academy.belhard.pizzachef.jsonobjects.OrderConvert;
import academy.belhard.pizzachef.model.entity.ClientEntity;
import academy.belhard.pizzachef.model.entity.OrderEntity;
import academy.belhard.pizzachef.model.json.OrderJson;
import com.google.gson.Gson;

import java.util.*;

public class RunCriteria {
    public static void main(String[] args) {
        List<OrderEntity> list = new OrderDAO().getRecordList();
        List<OrderJson> orderJsonList = new ArrayList<>();
        for (OrderEntity orderEntity : list) {
            OrderConvert orderConvert = new OrderConvert(orderEntity);
            orderJsonList.add(orderConvert.getOrderJson());
        }

        Gson gson = new Gson();
        String jsonResponse = gson.toJson(orderJsonList);
        System.out.println(jsonResponse);

        System.out.println("----------------------------------------------------------------------------------------");
        AbstractDAO<ClientEntity> clientDAO = new ClientDAO();
        ClientEntity clientEntity = clientDAO.getRecordById(1);
        System.out.println(clientEntity.getName());

        System.out.println("----------------------------------------------------------------------------------------");
        AbstractDAO<OrderEntity> orderDAO = new OrderDAO();
        Map<String, Object> map = new HashMap<>();
//        map.put("client", clientEntity);
        map.put("finished", false);
        List<OrderEntity> orderEntityList = orderDAO.getRecordList(map);
        for (OrderEntity orderEntity : orderEntityList) {
            System.out.println(orderEntity.getCreateDateTime());
        }

        OrderConvert oc = new OrderConvert(orderEntityList.get(0));
        OrderJson oj = oc.getOrderJson();
        oc = new OrderConvert(oj);

        OrderEntity order1 = orderDAO.getRecordById(oc.getOrderEntity().getId());
        System.out.println(order1.getId());

        orderDAO.deleteRecord(order1);

        OrderEntity order = orderDAO.getRecordById(1);
        System.out.println(order.getId());
    }
}

