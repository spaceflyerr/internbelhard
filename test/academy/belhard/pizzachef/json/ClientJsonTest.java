package academy.belhard.pizzachef.json;

import academy.belhard.pizzachef.model.json.AddressJson;
import academy.belhard.pizzachef.model.json.ClientJson;
import academy.belhard.pizzachef.model.json.PhoneJson;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.IsNot.not;

public class ClientJsonTest extends Assert {
    private static final long ID_SOURCES = 1;
    private static final String NAME_SOURCES = "Test name sources";
    private static final String AUTH_KEY_SOURCES = "KEY SOURCES";
    private static final AddressJson ADDRESS_SOURCES = new AddressJson();
    private static final List<PhoneJson> PHONE_LIST_SOURCES = new ArrayList<>();

    @Test
    public void testCloneWhereSourceObjectEqualTargetObject() {
        ClientJson source = new ClientJson();
        source.setName(NAME_SOURCES);
        source.setPhoneList(PHONE_LIST_SOURCES);
        source.setAddress(ADDRESS_SOURCES);
        source.setAuthKey(AUTH_KEY_SOURCES);
        source.setId(ID_SOURCES);

        ADDRESS_SOURCES.setCountry("Country sources");
        PhoneJson phone_sources = new PhoneJson();
        phone_sources.setPhone("Phone sources");
        phone_sources.setDepartment("Department sources");
        PHONE_LIST_SOURCES.add(phone_sources);

        ClientJson target = source.clone();

        assertEquals(source.getName(),target.getName());
        assertEquals(source.getAddress(),target.getAddress());
        assertEquals(source.getAuthKey(),target.getAuthKey());
        assertEquals(source.getId(),target.getId());
        assertEquals(source.getPhoneList(),target.getPhoneList());
    }

    @Test
    public void testCloneWhereSourceObjectNotModifyIfTargetObjectModify() {
        ClientJson source = new ClientJson();
        source.setName(NAME_SOURCES);
        source.setPhoneList(PHONE_LIST_SOURCES);
        source.setAddress(ADDRESS_SOURCES);
        source.setAuthKey(AUTH_KEY_SOURCES);
        source.setId(ID_SOURCES);

        ADDRESS_SOURCES.setCountry("Country sources");
        PhoneJson phone_sources = new PhoneJson();
        phone_sources.setPhone("Phone sources");
        phone_sources.setDepartment("Department sources");
        PHONE_LIST_SOURCES.add(phone_sources);

        ClientJson target = source.clone();

        target.setName("name target");
        PhoneJson phone_target = new PhoneJson();
        phone_target.setPhone("Phone target");
        phone_target.setDepartment("Department target");
        List<PhoneJson> pjt = new ArrayList<>();
        pjt.add(phone_target);
        target.setPhoneList(pjt);
        target.setAddress(new AddressJson());
        target.setAuthKey("Auth key target");
        target.setId(100);

        assertThat(source.getName(), not(target.getName()));
        assertEquals(source.getName(),NAME_SOURCES);

        assertThat(source.getAddress(), not(target.getAddress()));
        assertEquals(source.getAddress(),ADDRESS_SOURCES);

        assertThat(source.getAuthKey(), not(target.getAuthKey()));
        assertEquals(source.getAuthKey(),AUTH_KEY_SOURCES);

        assertThat(source.getId(), not(target.getId()));
        assertEquals(source.getId(),ID_SOURCES);

        assertThat(source.getPhoneList().get(0).getDepartment(), not(target.getPhoneList().get(0).getDepartment()));
        assertEquals(source.getPhoneList().get(0).getDepartment(),"Department sources");
    }
}
