package academy.belhard.pizzachef.json;

import academy.belhard.pizzachef.model.json.CategoryJson;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.core.IsNot.not;

public class CategoryJsonTest extends Assert {
    private static final String NAME_SOURCES = "Cheese sources";

    @Test
    public void testCloneWhereSourceObjectEqualTargetObject() {
        CategoryJson source = new CategoryJson();
        source.setName(NAME_SOURCES);

        CategoryJson target = source.clone();

        assertEquals(source.getName(),target.getName());
    }

    @Test
    public void testCloneWhereSourceObjectNotModifyIfTargetObjectModify() {
        CategoryJson source = new CategoryJson();
        source.setName(NAME_SOURCES);

        CategoryJson target = source.clone();

        target.setName("Cheese target");

        assertThat(source.getName(), not(target.getName()));
        assertEquals(source.getName(),NAME_SOURCES);
    }
}
