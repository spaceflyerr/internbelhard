package academy.belhard.pizzachef.json;

import academy.belhard.pizzachef.model.json.AddressJson;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.core.IsNot.not;

public class AddressJsonTest extends Assert{
    private static final String STREET_SOURCES = "Street";
    private static final String ZIP_SOURCES = "ZIP-CODE";
    private static final String BUILDING_SOURCES = "Building";
    private static final String CITY_SOURCES = "City";
    private static final String COUNTRY_SOURCES = "Country";
    private static final String OFFICE_SOURCES = "Office";
    private static final String STATE_SOURCES = "State";

    @Test
    public void testCloneWhereSourceObjectEqualTargetObject() {
        AddressJson source = new AddressJson();
        source.setStreet(STREET_SOURCES);
        source.setZip(ZIP_SOURCES);
        source.setBuilding(BUILDING_SOURCES);
        source.setCity(CITY_SOURCES);
        source.setCountry(COUNTRY_SOURCES);
        source.setOffice(OFFICE_SOURCES);
        source.setState(STATE_SOURCES);

        AddressJson target = source.clone();

        assertEquals(source.getBuilding(),target.getBuilding());
        assertEquals(source.getState(),target.getState());
        assertEquals(source.getStreet(),target.getStreet());
        assertEquals(source.getZip(),target.getZip());
        assertEquals(source.getCity(),target.getCity());
        assertEquals(source.getCountry(),target.getCountry());
        assertEquals(source.getOffice(),target.getOffice());
    }

    @Test
    public void testCloneWhereSourceObjectNotModifyIfTargetObjectModify() {
        AddressJson source = new AddressJson();
        source.setStreet(STREET_SOURCES);
        source.setZip(ZIP_SOURCES);
        source.setBuilding(BUILDING_SOURCES);
        source.setCity(CITY_SOURCES);
        source.setCountry(COUNTRY_SOURCES);
        source.setOffice(OFFICE_SOURCES);
        source.setState(STATE_SOURCES);

        AddressJson target = source.clone();

        target.setStreet("Street target");
        target.setZip("ZIP-CODE target");
        target.setBuilding("Building target");
        target.setCity("City target");
        target.setCountry("Country target");
        target.setOffice("Office target");
        target.setState("State target");

        assertThat(source.getBuilding(), not(target.getBuilding()));
        assertEquals(source.getBuilding(),BUILDING_SOURCES);

        assertThat(source.getState(), not(target.getState()));
        assertEquals(source.getState(),STATE_SOURCES);

        assertThat(source.getStreet(), not(target.getStreet()));
        assertEquals(source.getStreet(),STREET_SOURCES);

        assertThat(source.getZip(), not(target.getZip()));
        assertEquals(source.getZip(),ZIP_SOURCES);

        assertThat(source.getCity(), not(target.getCity()));
        assertEquals(source.getCity(),CITY_SOURCES);

        assertThat(source.getCountry(), not(target.getCountry()));
        assertEquals(source.getCountry(),COUNTRY_SOURCES);

        assertThat(source.getOffice(), not(target.getOffice()));
        assertEquals(source.getOffice(),OFFICE_SOURCES);
    }
}
