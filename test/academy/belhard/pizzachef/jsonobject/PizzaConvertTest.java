package academy.belhard.pizzachef.jsonobject;

import academy.belhard.pizzachef.jsonobjects.PizzaConvert;
import academy.belhard.pizzachef.model.entity.*;
import academy.belhard.pizzachef.model.json.PizzaJson;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class PizzaConvertTest extends Assert {
    private static final String STREET_SOURCES = "Street";
    private static final String ZIP_SOURCES = "ZIP-CODE";
    private static final String BUILDING_SOURCES = "Building";
    private static final String CITY_SOURCES = "City";
    private static final String COUNTRY_SOURCES = "Country";
    private static final String OFFICE_SOURCES = "Office";
    private static final String STATE_SOURCES = "State";

    @Test
    public void testConvertWhereEntityToJsonAndEqualBoth() {
        PizzaEntity entity = new PizzaEntity();
        entity.setId(1);

        PizzaTypeEntity pizzaType = new PizzaTypeEntity();
        pizzaType.setId(1);
        pizzaType.setDescription("Description source");
        pizzaType.setName("Name source");
        PizzaSizeEntity pizzaSize = new PizzaSizeEntity();
        pizzaSize.setId(1);
        pizzaSize.setDiameter(200);
        pizzaSize.setName("Name size sources");

        AddressEntity address = new AddressEntity();
        address.setCountry(COUNTRY_SOURCES);
        address.setId(1);
        address.setOffice(OFFICE_SOURCES);
        address.setBuilding(BUILDING_SOURCES);
        address.setStreet(STREET_SOURCES);
        address.setState(STATE_SOURCES);
        address.setCity(CITY_SOURCES);
        address.setZip(ZIP_SOURCES);

        PhoneEntity phone = new PhoneEntity();
        phone.setId(1);
        phone.setDepartment("Department source");
        phone.setPhone("Phone source");

        List<PhoneEntity> phoneList = new ArrayList<>();
        phoneList.add(phone);

        ContactEntity contact = new ContactEntity();
        contact.setId(1);
        contact.setAddress(address);
        contact.setPhoneList(phoneList);
        ClientEntity client = new ClientEntity();
        client.setId(1);
        client.setAuthKey("Auth key sources");
        client.setName("Name client sources");
        client.setContact(contact);

        OrderEntity order = new OrderEntity();
        order.setId(1);
        order.setFinished(false);
        order.setCreateDateTime(new Date());
        order.setClient(client);

        entity.setPizzaSize(pizzaSize);
        entity.setPizzaType(pizzaType);
        entity.setOrder(order);

        IngredientEntity ingredient = new IngredientEntity();
        ingredient.setId(1);
        ingredient.setName("Name ingredient sources");
        CategoryEntity category = new CategoryEntity();
        category.setId(1);
        category.setName("Name category sources");
        List<IngredientEntity> ingredientList = new ArrayList<>();
        ingredientList.add(ingredient);
        category.setListOfIngredients(ingredientList);
        ingredient.setCategory(category);

        Map<Byte, IngredientEntity> ingredientMap = new HashMap();
        ingredientMap.put((byte)1, ingredient);
        entity.setIngredientMap(ingredientMap);

        List<OrderEntity> orderList = new ArrayList<>();
        orderList.add(order);
        client.setListOfOrders(orderList);

        PizzaConvert convert = new PizzaConvert(entity);

        PizzaJson json = convert.getPizzaJson();

        assertEquals(json.getPizzaSize().getDiameter(), entity.getPizzaSize().getDiameter());
        assertEquals(json.getPizzaSize().getName(), entity.getPizzaSize().getName());
        assertEquals(json.getPizzaType().getDescription(), entity.getPizzaType().getDescription());
        assertEquals(json.getPizzaType().getName(), entity.getPizzaType().getName());
        assertEquals(json.getIngredientMap().size(), entity.getIngredientMap().size());
    }
}
