package academy.belhard.pizzachef.jsonobject;

import academy.belhard.pizzachef.jsonobjects.ClientConvert;
import academy.belhard.pizzachef.model.entity.*;
import academy.belhard.pizzachef.model.json.AddressJson;
import academy.belhard.pizzachef.model.json.ClientJson;
import academy.belhard.pizzachef.model.json.PhoneJson;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClientConvertTest extends Assert {
    private static final String STREET_SOURCES = "Street";
    private static final String ZIP_SOURCES = "ZIP-CODE";
    private static final String BUILDING_SOURCES = "Building";
    private static final String CITY_SOURCES = "City";
    private static final String COUNTRY_SOURCES = "Country";
    private static final String OFFICE_SOURCES = "Office";
    private static final String STATE_SOURCES = "State";

    private static final long ID_SOURCES = 1;
    private static final String NAME_SOURCES = "Test name sources";
    private static final String AUTH_KEY_SOURCES = "KEY SOURCES";
    private static final AddressJson ADDRESS_SOURCES = new AddressJson();
    private static final List<PhoneJson> PHONE_LIST_SOURCES = new ArrayList<>();

    @Test
    public void testConvertWhereEntityToJsonAndEqualBoth() {
        ClientEntity entity = new ClientEntity();
        entity.setId(ID_SOURCES);
        entity.setAuthKey(AUTH_KEY_SOURCES);
        entity.setName(NAME_SOURCES);

        AddressEntity address = new AddressEntity();
        address.setCountry(COUNTRY_SOURCES);
        address.setId(1);
        address.setOffice(OFFICE_SOURCES);
        address.setBuilding(BUILDING_SOURCES);
        address.setStreet(STREET_SOURCES);
        address.setState(STATE_SOURCES);
        address.setCity(CITY_SOURCES);
        address.setZip(ZIP_SOURCES);

        PhoneEntity phone = new PhoneEntity();
        phone.setId(1);
        phone.setDepartment("Department source");
        phone.setPhone("Phone source");

        List<PhoneEntity> phoneList = new ArrayList<>();
        phoneList.add(phone);
        ContactEntity contact = new ContactEntity();
        contact.setId(1);
        contact.setAddress(address);
        contact.setPhoneList(phoneList);
        entity.setContact(contact);
        phone.setContact(contact);

        OrderEntity order = new OrderEntity();
        order.setId(1);
        order.setFinished(false);
        order.setCreateDateTime(new Date());
        order.setClient(entity);

        PizzaTypeEntity pizzaType = new PizzaTypeEntity();
        pizzaType.setId(1);
        pizzaType.setDescription("Description source");
        pizzaType.setName("Name source");
        PizzaSizeEntity pizzaSize = new PizzaSizeEntity();
        pizzaSize.setId(1);
        pizzaSize.setDiameter(200);
        pizzaSize.setName("Name size sources");
        PizzaEntity pizza = new PizzaEntity();
        pizza.setId(1);
        pizza.setPizzaType(pizzaType);
        pizza.setPizzaSize(pizzaSize);
        List<PizzaEntity> pizzaList = new ArrayList<>();
        pizzaList.add(pizza);
        order.setListOfPizzas(pizzaList);
        List<OrderEntity> orderList = new ArrayList<>();
        orderList.add(order);
        entity.setListOfOrders(orderList);

        ClientConvert convert = new ClientConvert(entity);

        ClientJson json = convert.getClientJson();

        assertEquals(json.getName(), entity.getName());
        assertEquals(json.getId(), entity.getId());
        assertEquals(json.getAuthKey(), entity.getAuthKey());
        assertEquals(json.getAddress().getCountry(), entity.getContact().getAddress().getCountry());
        assertEquals(json.getPhoneList().get(0).getPhone(), entity.getContact().getPhoneList().get(0).getPhone());
    }
}
