package academy.belhard.pizzachef.jsonobject;

import academy.belhard.pizzachef.jsonobjects.PizzaSizeConvert;
import academy.belhard.pizzachef.model.entity.PizzaSizeEntity;
import academy.belhard.pizzachef.model.json.PizzaSizeJson;
import org.junit.Assert;
import org.junit.Test;

public class PizzaSizeConvertTest extends Assert {
    private static final String NAME_SOURCES = "Name size sources";
    private static final int SIZE_SOURCES = 200;

    @Test
    public void testConvertWhereEntityToJsonAndEqualBoth() {
        PizzaSizeEntity entity = new PizzaSizeEntity();
        entity.setName(NAME_SOURCES);
        entity.setDiameter(SIZE_SOURCES);
        entity.setId(1);

        PizzaSizeConvert convert = new PizzaSizeConvert(entity);

        PizzaSizeJson json = convert.getPizzaSizeJson();

        assertEquals(entity.getName(),json.getName());
        assertEquals(entity.getDiameter(),json.getDiameter());
    }

    @Test
    public void testConvertWhereJsonToEntityAndEqualBoth() {
        PizzaSizeJson json = new PizzaSizeJson();
        json.setName(NAME_SOURCES);
        json.setDiameter(SIZE_SOURCES);

        PizzaSizeConvert convert = new PizzaSizeConvert(json);

        PizzaSizeEntity entity = convert.getPizzaSizeEntity();

        assertEquals(entity.getName(),json.getName());
        assertEquals(entity.getDiameter(),json.getDiameter());
        assertEquals(0,entity.getId());
    }

}
