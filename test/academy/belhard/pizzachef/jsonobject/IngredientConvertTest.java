package academy.belhard.pizzachef.jsonobject;

import academy.belhard.pizzachef.jsonobjects.IngredientConvert;
import academy.belhard.pizzachef.model.entity.CategoryEntity;
import academy.belhard.pizzachef.model.entity.IngredientEntity;
import academy.belhard.pizzachef.model.json.CategoryJson;
import academy.belhard.pizzachef.model.json.IngredientJson;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class IngredientConvertTest extends Assert {
    private static final String NAME_SOURCES = "Name sources";

    @Test
    public void testConvertWhereEntityToJsonAndEqualBoth() {
        IngredientEntity entity = new IngredientEntity();
        entity.setId(1);
        entity.setName(NAME_SOURCES);

        CategoryEntity category = new CategoryEntity();
        category.setId(1);
        category.setName("Name category sources");
        entity.setCategory(category);
        List<IngredientEntity> ingredientList = new ArrayList<>();
        ingredientList.add(entity);
        category.setListOfIngredients(ingredientList);

        IngredientConvert convert = new IngredientConvert(entity);

        IngredientJson json = convert.getIngredientJson();

        assertEquals(entity.getName(), json.getName());
        assertEquals(entity.getCategory().getName(), json.getCategory().getName());
    }

    @Test
    public void testConvertWhereJsonToEntityAndEqualBoth() {
        IngredientJson json = new IngredientJson();
        json.setName(NAME_SOURCES);

        CategoryJson category = new CategoryJson();
        category.setName("Name category sources");
        json.setCategory(category);

        IngredientConvert convert = new IngredientConvert(json);

        IngredientEntity entity = convert.getIngredientEntity();

        assertEquals(json.getName(), entity.getName());
        assertEquals(json.getCategory().getName(), entity.getCategory().getName());
        assertEquals(0, entity.getId());
        assertEquals(0, entity.getCategory().getId());
        assertEquals(null, entity.getCategory().getListOfIngredients());
    }
}