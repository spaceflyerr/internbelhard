package academy.belhard.pizzachef.jsonobject;

import academy.belhard.pizzachef.jsonobjects.CategoryConvert;
import academy.belhard.pizzachef.model.entity.CategoryEntity;
import academy.belhard.pizzachef.model.json.CategoryJson;
import org.junit.Assert;
import org.junit.Test;

public class CategoryConvertTest extends Assert {
    private static final String NAME_SOURCES = "Cheese sources";

    @Test
    public void testConvertWhereEntityToJsonAndEqualBoth() {
        CategoryEntity entity = new CategoryEntity();
        entity.setName(NAME_SOURCES);
        entity.setId(1);

        CategoryConvert convert = new CategoryConvert(entity);

        CategoryJson json = convert.getCategoryJson();

        assertEquals(entity.getName(),json.getName());
    }

    @Test
    public void testConvertWhereJsonToEntityAndEqualBoth() {
        CategoryJson json = new CategoryJson();
        json.setName(NAME_SOURCES);

        CategoryConvert convert = new CategoryConvert(json);

        CategoryEntity entity = convert.getCategoryEntity();

        assertEquals(json.getName(),entity.getName());
        assertEquals(0,entity.getId());
    }

}
