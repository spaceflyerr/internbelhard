package academy.belhard.pizzachef.jsonobject;

import academy.belhard.pizzachef.jsonobjects.PizzaTypeConvert;
import academy.belhard.pizzachef.model.entity.PizzaTypeEntity;
import academy.belhard.pizzachef.model.json.PizzaTypeJson;
import org.junit.Assert;
import org.junit.Test;

public class PizzaTypeConvertTest extends Assert {
    private static final String NAME_SOURCES = "Name type sources";
    private static final String DESCRIPTION_SOURCES = "Description type sources";

    @Test
    public void testConvertWhereEntityToJsonAndEqualBoth() {
        PizzaTypeEntity entity = new PizzaTypeEntity();
        entity.setName(NAME_SOURCES);
        entity.setDescription(DESCRIPTION_SOURCES);
        entity.setId(1);

        PizzaTypeConvert convert = new PizzaTypeConvert(entity);

        PizzaTypeJson json = convert.getPizzaTypeJson();

        assertEquals(entity.getName(),json.getName());
        assertEquals(entity.getDescription(),json.getDescription());
    }

    @Test
    public void testConvertWhereJsonToEntityAndEqualBoth() {
        PizzaTypeJson json = new PizzaTypeJson();
        json.setName(NAME_SOURCES);
        json.setDescription(DESCRIPTION_SOURCES);

        PizzaTypeConvert convert = new PizzaTypeConvert(json);

        PizzaTypeEntity entity = convert.getPizzaTypeEntity();

        assertEquals(entity.getName(),json.getName());
        assertEquals(0,entity.getId());
    }

}
