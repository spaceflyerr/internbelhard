package academy.belhard.pizzachef.jsonobject;

import academy.belhard.pizzachef.jsonobjects.AddressConvert;
import academy.belhard.pizzachef.model.entity.AddressEntity;
import academy.belhard.pizzachef.model.json.AddressJson;
import org.junit.Assert;
import org.junit.Test;

public class AddressConvertTest extends Assert {
    private static final String STREET_SOURCES = "Street";
    private static final String ZIP_SOURCES = "ZIP-CODE";
    private static final String BUILDING_SOURCES = "Building";
    private static final String CITY_SOURCES = "City";
    private static final String COUNTRY_SOURCES = "Country";
    private static final String OFFICE_SOURCES = "Office";
    private static final String STATE_SOURCES = "State";

    @Test
    public void testConvertWhereEntityToJsonAndEqualBoth() {
        AddressEntity entity = new AddressEntity();
        entity.setStreet(STREET_SOURCES);
        entity.setZip(ZIP_SOURCES);
        entity.setBuilding(BUILDING_SOURCES);
        entity.setCity(CITY_SOURCES);
        entity.setCountry(COUNTRY_SOURCES);
        entity.setOffice(OFFICE_SOURCES);
        entity.setState(STATE_SOURCES);
        entity.setId(1);

        AddressConvert convert = new AddressConvert(entity);

        AddressJson json = convert.getAddressJson();

        assertEquals(entity.getBuilding(),json.getBuilding());
        assertEquals(entity.getState(),json.getState());
        assertEquals(entity.getStreet(),json.getStreet());
        assertEquals(entity.getZip(),json.getZip());
        assertEquals(entity.getCity(),json.getCity());
        assertEquals(entity.getCountry(),json.getCountry());
        assertEquals(entity.getOffice(),json.getOffice());
    }

    @Test
    public void testConvertWhereJsonToEntityAndEqualBoth() {
        AddressJson json = new AddressJson();
        json.setStreet(STREET_SOURCES);
        json.setZip(ZIP_SOURCES);
        json.setBuilding(BUILDING_SOURCES);
        json.setCity(CITY_SOURCES);
        json.setCountry(COUNTRY_SOURCES);
        json.setOffice(OFFICE_SOURCES);
        json.setState(STATE_SOURCES);

        AddressConvert convert = new AddressConvert(json);

        AddressEntity entity = convert.getAddressEntity();

        assertEquals(json.getBuilding(),entity.getBuilding());
        assertEquals(json.getState(),entity.getState());
        assertEquals(json.getStreet(),entity.getStreet());
        assertEquals(json.getZip(),entity.getZip());
        assertEquals(json.getCity(),entity.getCity());
        assertEquals(json.getCountry(),entity.getCountry());
        assertEquals(json.getOffice(),entity.getOffice());
        assertEquals(0,entity.getId());
    }
}
