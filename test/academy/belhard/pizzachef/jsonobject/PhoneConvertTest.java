package academy.belhard.pizzachef.jsonobject;

import academy.belhard.pizzachef.jsonobjects.PhoneConvert;
import academy.belhard.pizzachef.model.entity.ContactEntity;
import academy.belhard.pizzachef.model.entity.PhoneEntity;
import academy.belhard.pizzachef.model.json.PhoneJson;
import org.junit.Assert;
import org.junit.Test;

public class PhoneConvertTest extends Assert {
    private static final String DEPARTMENT_SOURCES = "Department sources";
    private static final String PHONE_SOURCES = "Phone sources";

    @Test
    public void testConvertWhereEntityToJsonAndEqualBoth() {
        PhoneEntity entity = new PhoneEntity();
        entity.setDepartment(DEPARTMENT_SOURCES);
        entity.setPhone(PHONE_SOURCES);
        entity.setId(1);
        ContactEntity contact = new ContactEntity();
        entity.setContact(contact);

        PhoneConvert convert = new PhoneConvert(entity);

        PhoneJson json = convert.getPhoneJson();

        assertEquals(entity.getPhone(),json.getPhone());
        assertEquals(entity.getDepartment(),json.getDepartment());
    }

    @Test
    public void testConvertWhereJsonToEntityAndEqualBoth() {
        PhoneJson json = new PhoneJson();
        json.setDepartment(DEPARTMENT_SOURCES);
        json.setPhone(PHONE_SOURCES);

        PhoneConvert convert = new PhoneConvert(json);

        PhoneEntity entity = convert.getPhoneEntity();

        assertEquals(json.getPhone(),entity.getPhone());
        assertEquals(json.getDepartment(),entity.getDepartment());
        assertEquals(0,entity.getId());
        assertEquals(null, entity.getContact());
    }

}
