package academy.belhard.pizzachef.dao;

import academy.belhard.pizzachef.model.entity.PizzaTypeEntity;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.core.IsNot.not;

public class PizzaTypeDAOTest extends Assert {
    private static final String NAME_SOURCE = "Test name";
    private static final String DESCRIPTION_SOURCE = "Description test";
    private static final String DESCRIPTION_UPDATE_SOURCE = "Description update test";

    @Test
    public void testAddNewRecordInPizzaType() {
        PizzaTypeEntity entity = new PizzaTypeEntity();
        entity.setDescription(DESCRIPTION_SOURCE);
        entity.setName(NAME_SOURCE);

        AbstractDAO<PizzaTypeEntity> dao = new PizzaTypeDAO();
        dao.addRecord(entity);
        Map<String, Object> map = new HashMap<>();
        map.put("name", NAME_SOURCE);
        map.put("description", DESCRIPTION_SOURCE);
        List<PizzaTypeEntity> list =  dao.getRecordList(map);

        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getDescription(), DESCRIPTION_SOURCE);
        assertEquals(list.get(0).getName(), NAME_SOURCE);
        assertThat(list.get(0).getId(), not(new Long(0)));
    }

    @Test
    public void testUpdateNewRecordInPizzaType() {
        AbstractDAO<PizzaTypeEntity> dao = new PizzaTypeDAO();
        Map<String, Object> map = new HashMap<>();
        map.put("name", NAME_SOURCE);
        map.put("description", DESCRIPTION_SOURCE);
        PizzaTypeEntity pizzaType =  dao.getRecordList(map).get(0);
        pizzaType.setDescription(DESCRIPTION_UPDATE_SOURCE);
        dao.updateRecord(pizzaType);

        Map<String, Object> mapNew = new HashMap<>();
        mapNew.put("name", NAME_SOURCE);
        List<PizzaTypeEntity> list =  dao.getRecordList(mapNew);

        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getDescription(), DESCRIPTION_UPDATE_SOURCE);
        assertEquals(list.get(0).getName(), NAME_SOURCE);
        assertThat(list.get(0).getId(), not(new Long(0)));
    }

    @Test
    public void testDeleteNewRecordInPizzaType() {
        AbstractDAO<PizzaTypeEntity> dao = new PizzaTypeDAO();
        Map<String, Object> map = new HashMap<>();
        map.put("name", NAME_SOURCE);
        map.put("description", DESCRIPTION_UPDATE_SOURCE);
        PizzaTypeEntity pizzaType =  dao.getRecordList(map).get(0);
        dao.deleteRecord(pizzaType);

        List<PizzaTypeEntity> list =  dao.getRecordList();
        assertEquals(list.size(), 3);

    }
}
