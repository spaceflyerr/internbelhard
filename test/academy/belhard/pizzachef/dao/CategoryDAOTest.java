package academy.belhard.pizzachef.dao;

import academy.belhard.pizzachef.model.entity.CategoryEntity;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.core.IsNot.not;

public class CategoryDAOTest extends Assert {
    private static final String NAME_SOURCE = "Test name";
    private static final String NAME_UPDATE_SOURCE = "Test update name";

    @Test
    public void testAddNewRecordInCategory() {
        CategoryEntity entity = new CategoryEntity();
        entity.setListOfIngredients(null);
        entity.setName(NAME_SOURCE);

        AbstractDAO<CategoryEntity> dao = new CategoryDAO();
        dao.addRecord(entity);
        Map<String, Object> map = new HashMap<>();
        map.put("name", NAME_SOURCE);
        List<CategoryEntity> list =  dao.getRecordList(map);

        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getName(), NAME_SOURCE);
        assertThat(list.get(0).getId(), not(new Long(0)));
    }

    @Test
    public void testUpdateNewRecordInCategory() {
        AbstractDAO<CategoryEntity> dao = new CategoryDAO();
        Map<String, Object> map = new HashMap<>();
        map.put("name", NAME_SOURCE);
        CategoryEntity category =  dao.getRecordList(map).get(0);
        category.setName(NAME_UPDATE_SOURCE);
        dao.updateRecord(category);

        Map<String, Object> mapNew = new HashMap<>();
        mapNew.put("name", NAME_UPDATE_SOURCE);
        List<CategoryEntity> list =  dao.getRecordList(mapNew);

        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getListOfIngredients(), null);
        assertEquals(list.get(0).getName(), NAME_UPDATE_SOURCE);
        assertThat(list.get(0).getId(), not(new Long(0)));
    }

    @Test
    public void testDeleteNewRecordInCategory() {
        AbstractDAO<CategoryEntity> dao = new CategoryDAO();
        Map<String, Object> map = new HashMap<>();
        map.put("name", NAME_UPDATE_SOURCE);
        CategoryEntity category =  dao.getRecordList(map).get(0);
        dao.deleteRecord(category);

        List<CategoryEntity> list =  dao.getRecordList();
        assertEquals(list.size(), 2);

    }

}
