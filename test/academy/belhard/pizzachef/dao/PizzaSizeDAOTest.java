package academy.belhard.pizzachef.dao;

import academy.belhard.pizzachef.model.entity.PizzaSizeEntity;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.core.IsNot.not;

public class PizzaSizeDAOTest extends Assert {
    private static final int DIAMETER_SOURCE = 10;
    private static final int DIAMETER_UPDATE_SOURCE = 20;
    private static final String NAME_SOURCE = "Test name";

    @Test
    public void testAddNewRecordInPizzaSize() {
        PizzaSizeEntity entity = new PizzaSizeEntity();
        entity.setDiameter(DIAMETER_SOURCE);
        entity.setName(NAME_SOURCE);

        AbstractDAO<PizzaSizeEntity> dao = new PizzaSizeDAO();
        dao.addRecord(entity);
        Map<String, Object> map = new HashMap<>();
        map.put("name", NAME_SOURCE);
        map.put("diameter", DIAMETER_SOURCE);
        List<PizzaSizeEntity> list =  dao.getRecordList(map);

        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getDiameter(), DIAMETER_SOURCE);
        assertEquals(list.get(0).getName(), NAME_SOURCE);
        assertThat(list.get(0).getId(), not(new Long(0)));
    }

    @Test
    public void testUpdateNewRecordInPizzaSize() {
        AbstractDAO<PizzaSizeEntity> dao = new PizzaSizeDAO();
        Map<String, Object> map = new HashMap<>();
        map.put("name", NAME_SOURCE);
        map.put("diameter", DIAMETER_SOURCE);
        PizzaSizeEntity pizzaSize =  dao.getRecordList(map).get(0);
        pizzaSize.setDiameter(DIAMETER_UPDATE_SOURCE);
        dao.updateRecord(pizzaSize);

        Map<String, Object> mapNew = new HashMap<>();
        mapNew.put("name", NAME_SOURCE);
        List<PizzaSizeEntity> list =  dao.getRecordList(mapNew);

        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getDiameter(), DIAMETER_UPDATE_SOURCE);
        assertEquals(list.get(0).getName(), NAME_SOURCE);
        assertThat(list.get(0).getId(), not(new Long(0)));
    }

    @Test
    public void testDeleteNewRecordInPizzaSize() {
        AbstractDAO<PizzaSizeEntity> dao = new PizzaSizeDAO();
        Map<String, Object> map = new HashMap<>();
        map.put("name", NAME_SOURCE);
        map.put("diameter", DIAMETER_UPDATE_SOURCE);
        PizzaSizeEntity pizzaSize =  dao.getRecordList(map).get(0);
        dao.deleteRecord(pizzaSize);

        List<PizzaSizeEntity> list =  dao.getRecordList();
        assertEquals(list.size(), 3);

    }
}
