package academy.belhard.pizzachef.dao;

import academy.belhard.pizzachef.model.entity.AddressEntity;
import academy.belhard.pizzachef.model.entity.ClientEntity;
import academy.belhard.pizzachef.model.entity.ContactEntity;
import academy.belhard.pizzachef.model.entity.PhoneEntity;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.core.IsNot.not;

public class ClientDAOTest extends Assert {
    private static final String STREET_SOURCES = "Street";
    private static final String ZIP_SOURCES = "ZIP-CODE";
    private static final String BUILDING_SOURCES = "Building";
    private static final String CITY_SOURCES = "City";
    private static final String COUNTRY_SOURCES = "Country";
    private static final String OFFICE_SOURCES = "Office";
    private static final String STATE_SOURCES = "State";

    private static final String NAME_SOURCE = "Test name";
    private static final String NAME_UPDATE_SOURCE = "Test update name";
    private static final String AUTH_KEY_SOURCE = "Test auth key";
    private static final String PHONE_SOURCE = "Test phone";
    private static final String PHONE_DEPARTMENT = "Test department";

    @Test
    public void testAddNewRecordInClient() {
        ClientEntity entity = new ClientEntity();
        entity.setId(0);
        entity.setName(NAME_SOURCE);
        entity.setAuthKey(AUTH_KEY_SOURCE);
        entity.setListOfOrders(null);

        AddressEntity address = new AddressEntity();
        address.setStreet(STREET_SOURCES);
        address.setZip(ZIP_SOURCES);
        address.setBuilding(BUILDING_SOURCES);
        address.setCity(CITY_SOURCES);
        address.setCountry(COUNTRY_SOURCES);
        address.setOffice(OFFICE_SOURCES);
        address.setState(STATE_SOURCES);
        address.setId(0);

        PhoneEntity phone = new PhoneEntity();
        phone.setId(0);
        phone.setPhone(PHONE_SOURCE);
        phone.setDepartment(PHONE_DEPARTMENT);

        List<PhoneEntity> phoneList = new ArrayList<>();
        phoneList.add(phone);

        ContactEntity contact = new ContactEntity();
        contact.setId(0);
        contact.setAddress(address);
        contact.setPhoneList(phoneList);
        entity.setContact(contact);
        phone.setContact(contact);

        AbstractDAO<ClientEntity> dao = new ClientDAO();
        dao.addRecord(entity);
        Map<String, Object> map = new HashMap<>();
        map.put("name", NAME_SOURCE);
        List<ClientEntity> list =  dao.getRecordList(map);

        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getName(), NAME_SOURCE);
        assertThat(list.get(0).getId(), not(new Long(0)));
    }

    @Test
    public void testUpdateNewRecordInClient() {
        AbstractDAO<ClientEntity> dao = new ClientDAO();
        Map<String, Object> map = new HashMap<>();
        map.put("name", NAME_SOURCE);
        ClientEntity client =  dao.getRecordList(map).get(0);
        client.setName(NAME_UPDATE_SOURCE);
        dao.updateRecord(client);

        Map<String, Object> mapNew = new HashMap<>();
        mapNew.put("name", NAME_UPDATE_SOURCE);
        List<ClientEntity> list =  dao.getRecordList(mapNew);

        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getName(), NAME_UPDATE_SOURCE);
        assertThat(list.get(0).getId(), not(new Long(0)));
    }

    @Test
    public void testDeleteNewRecordInClient() {
        AbstractDAO<ClientEntity> dao = new ClientDAO();
        Map<String, Object> map = new HashMap<>();
        map.put("name", NAME_UPDATE_SOURCE);
        ClientEntity client =  dao.getRecordList(map).get(0);
        dao.deleteRecord(client);

        List<ClientEntity> list =  dao.getRecordList();
        assertEquals(list.size(), 1);

    }
}
